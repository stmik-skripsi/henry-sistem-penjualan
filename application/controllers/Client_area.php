<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_area extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function index()
	{
		if ($this->session->userdata('status')!="loggedin") {
			redirect('client_area/login');
		}else{
			$data['page_title'] = 'Dashboard';
			$data['current'] = 'Dashboard';
			$this->load->view('backend/base/header',$data);
			$this->load->view('backend/services',$data);
			$this->load->view('backend/base/footer');
		}
	}

	function login()
	{
		if ($this->session->userdata('status')!="loggedin") {
			$data['page_title'] = 'Login';
			$this->load->view('backend/base/simpleheader',$data);
			$this->load->view('backend/signin');
			$this->load->view('backend/base/footer');
		}else{
			redirect('client_area');
		}
	}

	function register()
	{
		$data['page_title'] = 'Register';
		$this->load->view('backend/base/simpleheader',$data);
		$this->load->view('backend/signup');
		$this->load->view('backend/base/footer');
	}

	function auth()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$where = array(
			'email' => $email,
			'password' => md5($password)
		);

		$this->load->model('m_login');
		$cek = $this->m_login->login_check("user",$where)->num_rows();

		if($cek > 0){

			$data_session = array(
				'email' => $email,
				'status' => "loggedin"
			);

			$this->session->set_userdata($data_session);
			redirect('client_area');

		}else{
			redirect('client_area');
		}
	}

	function auth2()
	{
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');

		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$now = date('d M y - H:i');

		$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
		$cek = $query->num_rows();

		if ($cek) {
			echo "Email sudah terdaftar";
		}else{
			$query = $this->db->query("INSERT INTO user VALUES ('','".$fname."','".$lname."','".$email."','".$password."','','','','".$now."')");
			redirect('client_area');
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect();
	}

	function profile_view()
	{
		$data['page_title'] = 'My Profile';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/profile_view');
		$this->load->view('backend/base/footer');
	}

	function order()
	{
		$data['page_title'] = 'Order';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/listjasa');
		$this->load->view('backend/base/footer');
	}

	function help()
	{
		$data['page_title'] = 'Help Center';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/helpcenter');
		$this->load->view('backend/base/footer');
	}

	function contact()
	{
		$data['page_title'] = 'Contact';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/invoice');
		$this->load->view('backend/base/footer');
	}

	function order_ads()
	{
		$data['page_title'] = 'Order Ads Wizard';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/order_ads');
		//$this->load->view('backend/base/footer');
	}

	function order_website()
	{
		$data['page_title'] = 'Order Website Wizard';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/order_website');
		//$this->load->view('backend/base/footer');
	}

	function order_seo()
	{
		$data['page_title'] = 'Order SEO Wizard';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/order_seo');
		//$this->load->view('backend/base/footer');
	}

	function generate_invoice_ads()
	{
		$adstype = $this->input->post('adstype');
		$adsplatform = $this->input->post('adsplatform');
		$adsage = $this->input->post('adsage');
		$adsgender = $this->input->post('adsgender');
		$adslocation = $this->input->post('adslocation');
		$adsjob = $this->input->post('adsjob');
		$adsbudget = $this->input->post('adsbudget');
		$adsduration = $this->input->post('adsduration');
		$adspayment = $this->input->post('adspayment');

		$amount = ($adsbudget * $adsduration) + 100000;

		$email = $this->session->userdata('email');
		$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
		$user = $query->row();

		$uid = $user->id;
		#$sid = $this->input->post('email');
		#$expiry =  $this->input->post('email');

		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$now = date('d M y - H:i');
		$now2 = date('F d, Y');

		$invoice = date('ymdHis');

		$data = array(
			'iduser' => $uid,
			'adstype' => $adstype,
			'adsplatform' => $adsplatform,
			'age' => $adsage,
			'gender' => $adsgender,
			'location' => $adslocation,
			'job' => $adsjob,
			'budget' => $adsbudget,
			'duration' => $adsduration,
			'invoicenum' => 'A'.$invoice,
		);

		$this->db->insert('services_ads', $data);

		if ($phone =! '') {
			$data2 = array(
				'phone' => $phone,
			);
			$this->db->update('user', $data2, "id = '".$uid."'");
		}else{}

		$query2 = $this->db->query("SELECT * FROM services_ads where invoicenum = 'A".$invoice."'");
		$servicesid = $query2->row();

		$data3 = array(
			'iduser' => $uid,
			'invoicenum' => 'A'.$invoice,
			'servicesname' => 'Ads',
			'servicesid' => $servicesid->id,
			'date' => $now2,
			'status' => 'UNPAID',
			'method' => $adspayment,
			'amount' => $amount,
		);
		$this->db->insert('invoice', $data3);
		redirect('client_area/invoice/A'.$invoice);
	}	

	function generate_invoice_web()
	{
		$webtype = $this->input->post('webtype');
		$webdomain = $this->input->post('webdomain');
		$webcolor = $this->input->post('webcolor');
		$webname = $this->input->post('webname');
		$webref = $this->input->post('webref');
		$webbrief = $this->input->post('webbrief');
		$webpayment = $this->input->post('webpayment');
		$phone = $this->input->post('phone');

		$onepage = 1000000;
		$companyprofile = 4000000;
		$onlineshop = 8000000;

		if ($webtype == "One Page") {
			$amount = $onepage;
		}elseif ($webtype == "Company Profile") {
			$amount = $companyprofile;
		}elseif ($webtype == "Online Shop") {
			$amount = $onlineshop;
		}

		$email = $this->session->userdata('email');
		$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
		$user = $query->row();

		$uid = $user->id;
		#$sid = $this->input->post('email');
		#$expiry =  $this->input->post('email');

		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$now = date('d M y - H:i');
		$now2 = date('F d, Y');

		$invoice = date('ymdHis');

		$data = array(
			'iduser' => $uid,
			'webtype' => $webtype,
			'webdomain' => $webdomain,
			'webcolor' => $webcolor,
			'webname' => $webname,
			'webref' => $webref,
			'webbrief' => $webbrief,
			'invoicenum' => $invoice,
			'invoicenum' => 'W'.$invoice,
		);

		$this->db->insert('services_website', $data);

		if ($phone =! '') {
			$data2 = array(
				'phone' => $phone,
			);
			$this->db->update('user', $data2, "id = '".$uid."'");
		}else{}

		$query2 = $this->db->query("SELECT * FROM services_website where invoicenum = 'W".$invoice."'");
		$servicesid = $query2->row();

		$data3 = array(
			'iduser' => $uid,
			'invoicenum' => 'W'.$invoice,
			'servicesname' => 'Website',
			'servicesid' => $servicesid->id,
			'date' => $now2,
			'status' => 'UNPAID',
			'method' => $webpayment,
			'amount' => $amount,
		);
		$this->db->insert('invoice', $data3);
		redirect('client_area/invoice/W'.$invoice);
	}	

	function generate_invoice_seo()
	{
		$seowebsite = $this->input->post('seowebsite');
		$seotopic = $this->input->post('seotopic');
		$seokeyword = $this->input->post('seokeyword');
		$seoperweek = $this->input->post('seoperweek');
		$seopayment = $this->input->post('seopayment');
		$seoduration = $this->input->post('seoduration');
		$phone = $this->input->post('phone');

		$priceperarticle = 50000;
		$amount = ($seoduration * 4 ) * ($priceperarticle * $seoperweek);

		$email = $this->session->userdata('email');
		$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
		$user = $query->row();

		$uid = $user->id;
		#$sid = $this->input->post('email');
		#$expiry =  $this->input->post('email');

		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$now = date('MM d y - H:i');
		$now2 = date('F d, Y');
		$invoice = date('ymdHis');

		$data = array(
			'iduser' => $uid,
			'seowebsite' => $seowebsite,
			'seotopic' => $seotopic,
			'seokeyword' => $seokeyword,
			'seoperweek' => $seoperweek,
			'seoduration' => $seoduration,
			'invoicenum' => 'S'.$invoice,
		);

		$this->db->insert('services_seo', $data);

		if ($phone =! '') {
			$data2 = array(
				'phone' => $phone,
			);
			$this->db->update('user', $data2, "id = '".$uid."'");
		}else{}

		$query2 = $this->db->query("SELECT * FROM services_seo where invoicenum = 'S".$invoice."'");
		$servicesid = $query2->row();

		$data3 = array(
			'iduser' => $uid,
			'invoicenum' => 'S'.$invoice,
			'servicesname' => 'SEO',
			'servicesid' => $servicesid->id,
			'date' => $now2,
			'status' => 'UNPAID',
			'method' => $seopayment,
			'amount' => $amount,
		);
		$this->db->insert('invoice', $data3);
		redirect('client_area/invoice/S'.$invoice);
	}

	function invoice_list(){
		$data['page_title'] = 'Invoice List';
		$this->load->view('backend/base/header',$data);
		$this->load->view('backend/invoicelist');
		//$this->load->view('backend/base/footer');
	}

	function invoice()
	{
		if ($this->uri->segment(3) == null) {
			redirect('client_area/invoice_list');

		}elseif ($this->uri->segment(3) != null) {
			$invoicenum = $this->uri->segment(3);

			$where = $invoicenum;
			$query = $this->db->query("SELECT * FROM invoice where invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {
				$query = $this->db->query("SELECT * FROM invoice where invoicenum = '".$invoicenum."'");
				$row = $query->row(1);

				$qseo = $this->db->query("SELECT * FROM services_seo where invoicenum = '".$invoicenum."'");
				$seo = $qseo->row(1);

				$qweb = $this->db->query("SELECT * FROM services_website where invoicenum = '".$invoicenum."'");
				$web = $qweb->row(1);

				$qads = $this->db->query("SELECT * FROM services_ads where invoicenum = '".$invoicenum."'");
				$ads = $qads->row(1);

				if ($row->servicesname == "SEO") {
					$data = array(
						'page_title' => 'Invoice #'.$invoicenum,
						'invoicenum' => $row->invoicenum,
						'status' => $row->status,
						'date' => $row->date,
						'method' => $row->method,
						'services' => $row->servicesname,
						'amount' => $row->amount,
						'seoperweek' => $seo->seoperweek,
						'seoduration' => $seo->seoduration,
					);
				}

				if ($row->servicesname == "Website") {
					$data = array(
						'page_title' => 'Invoice #'.$invoicenum,
						'invoicenum' => $row->invoicenum,
						'status' => $row->status,
						'date' => $row->date,
						'method' => $row->method,
						'services' => $row->servicesname,
						'amount' => $row->amount,
						'webtype' => $web->webtype,
						'webdomain' => $web->webdomain,
					);
				}

				if ($row->servicesname == "Ads") {
					$data = array(
						'page_title' => 'Invoice #'.$invoicenum,
						'invoicenum' => $row->invoicenum,
						'status' => $row->status,
						'date' => $row->date,
						'method' => $row->method,
						'services' => $row->servicesname,
						'amount' => $row->amount,
						'adstype' => $ads->adstype,
						'adsplatform' => $ads->adsplatform,
						'adsbudget' => $ads->budget,
						'adsduration' => $ads->duration,
					);
				}

				$this->load->view('backend/base/header',$data);
				$this->load->view('backend/invoice',$data);
				$this->load->view('backend/base/footer');			

			}else{
				redirect('client_area/invoice_list');
			}
		}

	}

	function confirm_payment()
	{
		if ($this->uri->segment(3) == null) {
			redirect('client_area/invoice_list');

		}elseif ($this->uri->segment(3) != null) {
			$invoicenum = $this->uri->segment(3);

			$query = $this->db->query("SELECT * FROM invoice WHERE invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {

				$data = array(
					'status' => "PENDING CONFIRMATION",
				);

				$this->db->where('invoicenum', $invoicenum);
				$this->db->update('invoice', $data);

				redirect('client_area/invoice/'.$invoicenum);	

			}else{
				redirect('client_area/invoice_list');
			}
		}
	}

	function services()
	{
		if ($this->uri->segment(3) == null) {

			$data['page_title'] = 'My Services';
			$data['current'] = 'My Services';
			$this->load->view('backend/base/header',$data);
			$this->load->view('backend/services',$data);
			$this->load->view('backend/base/footer');

		}elseif ($this->uri->segment(3) != null) {
			$invoicenum = $this->uri->segment(3);

			$query = $this->db->query("SELECT * FROM invoice WHERE invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {

				$query = $this->db->query("SELECT * FROM invoice where invoicenum = '".$invoicenum."'");
				$row = $query->row(1);

				if ($row->servicesname == "SEO") {
					$data['page_title'] = 'SEO Dashboard';
					$data['current'] = 'SEO Dashboard';
					$this->load->view('backend/base/header',$data);
					$this->load->view('backend/dashboard2',$data);
					$this->load->view('backend/base/footer');	
				}

				if ($row->servicesname == "Website") {
					$data['page_title'] = 'Website Dashboard';
					$data['current'] = 'Website Dashboard';
					$this->load->view('backend/base/header',$data);
					$this->load->view('backend/dashboard2',$data);
					$this->load->view('backend/base/footer');	
				}

				if ($row->servicesname == "Ads") {
					$data['page_title'] = 'Ads Dashboard';
					$data['current'] = 'Ads Dashboard';
					$this->load->view('backend/base/header',$data);
					$this->load->view('backend/dashboard',$data);
					$this->load->view('backend/base/footer');	
				}

			}else{
				$data['page_title'] = 'Client Area';
				$this->load->view('backend/base/header',$data);
				$this->load->view('backend/dashboard');
				$this->load->view('backend/base/footer');	
			}
		}	
	}
}
