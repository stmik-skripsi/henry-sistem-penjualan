<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom404 extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		if ($this->uri->segment(1) == 'client_area'){
			$data['page_title'] = '404';
			$this->load->view('backend/base/header',$data);
			$this->load->view('backend/error404');
			$this->load->view('backend/base/404footer');
		}elseif ($this->uri->segment(1) == 'admin_area') {
			$data['page_title'] = '404';
			$this->load->view('backend/base/header',$data);
			$this->load->view('backend/error404');
			$this->load->view('backend/base/404footer');
		}elseif ($this->uri->segment(1) == '') {
			$data['page_title'] = '404';
			$this->load->view('frontend/base/header',$data);
			$this->load->view('frontend/error404');
			$this->load->view('frontend/base/404footer');
		}
	}
}
