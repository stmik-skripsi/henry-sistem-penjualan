<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_area extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		if ($this->session->userdata('status')!="adminloggedin") {
			redirect('admin_area/login');
		}else{
			$data['page_title'] = 'Dashboard';
			$this->load->view('backend/base/adminheader',$data);
			$this->load->view('backend/admindashboard');
			$this->load->view('backend/base/adminfooter');
		}
	}

	function login()
	{
		if ($this->session->userdata('status')!="adminloggedin") {
			$data['page_title'] = 'Login';
			$this->load->view('backend/base/simpleheader',$data);
			$this->load->view('backend/adminsignin');
			$this->load->view('backend/base/footer');
		}else{
			redirect('admin_area');
		}
	}

	function auth()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$where = array(
			'email' => $email,
			'password' => md5($password)
		);

		$this->load->model('m_login');
		$cek = $this->m_login->login_check("admin",$where)->num_rows();

		if($cek > 0){

			$data_session = array(
				'email' => $email,
				'status' => "adminloggedin"
			);

			$this->session->set_userdata($data_session);
			redirect('admin_area');

		}else{
			redirect('admin_area');
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect();
	}

	function website_analytics()
	{
		$data['page_title'] = 'Website Analytics';
		$this->load->view('backend/base/adminheader',$data);
		$this->load->view('backend/admindashboard2');
		$this->load->view('backend/base/adminfooter');
	}

	function user_list()
	{
		if ($this->uri->segment(3) == null) {
			$data = array(
				'page_title' => 'User List',
				'ufname' => null,
				'ulname' => null,
				'uphone' => null,
				'uemail' => null,
				'ucompany' => null,
				'uposition' => null,
				'uid' => $this->uri->segment(3),
			);
			$this->load->view('backend/base/adminheader',$data);
			$this->load->view('backend/adminlistuser');
			$this->load->view('backend/base/adminfooter');

		}elseif ($this->uri->segment(3) != null){

			$id = $this->uri->segment(3);
			$query = $this->db->query("SELECT * FROM user where id = '".$id."'");
			$row = $query->row(1);

			$data = array(
				'page_title' => 'User List',
				'ufname' => $row->fname,
				'ulname' => $row->lname,
				'uphone' => $row->phone,
				'uemail' => $row->email,
				'ucompany' => $row->company,
				'uposition' => $row->position,
				'uid' => $this->uri->segment(3),
			);
			//$data['page_title'] = 'User List';

			$this->load->view('backend/base/adminheader',$data);
			$this->load->view('backend/adminlistuser');
			$this->load->view('backend/base/adminfooter');
		}
	}

	function admin_list()
	{
		if ($this->uri->segment(3) == null) {
			$data = array(
				'page_title' => 'Admin List',
				'uname' => null,
				'uphone' => null,
				'uemail' => null,
				'uposition' => null,
				'uid' => $this->uri->segment(3),
			);
			$this->load->view('backend/base/adminheader',$data);
			$this->load->view('backend/adminlistadmin');
			$this->load->view('backend/base/adminfooter');

		}elseif ($this->uri->segment(3) != null){

			$id = $this->uri->segment(3);
			$query = $this->db->query("SELECT * FROM admin where id = '".$id."'");
			$row = $query->row(1);

			$data = array(
				'page_title' => 'User List',
				'uname' => $row->name,
				'uphone' => $row->phone,
				'uemail' => $row->email,
				'uposition' => $row->position,
				'uid' => $this->uri->segment(3),
			);
			//$data['page_title'] = 'User List';

			$this->load->view('backend/base/adminheader',$data);
			$this->load->view('backend/adminlistadmin',$data);
			$this->load->view('backend/base/adminfooter');
		}
	}

	function delete_user()
	{
		$id = $this->uri->segment(3);
		$this->db->delete('user', array('id' => $id));
		redirect('admin_area/user_list');
	}

	function delete_admin()
	{
		$id = $this->uri->segment(3);
		$this->db->delete('admin', array('id' => $id));
		redirect('admin_area/admin_list');
	}

	function add_user()
	{
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$phone = $this->input->post('phone');
		$company = $this->input->post('company');
		$position = $this->input->post('position');

		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$now = date('d M y - H:i');

		$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
		$cek = $query->num_rows();

		if ($cek) {
			echo "Email sudah terdaftar";
		}else{
			$query = $this->db->query("INSERT INTO user VALUES ('','".$fname."','".$lname."','".$email."','".$password."','".$phone."','".$company."','".$position."','".$now."')");
			redirect('admin_area/user_list');
		}
	}

	function add_admin()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));
		$phone = $this->input->post('phone');
		$position = $this->input->post('position');

		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$now = date('d M y - H:i');

		$query = $this->db->query("SELECT * FROM admin where email = '".$email."'");
		$cek = $query->num_rows();

		if ($cek) {
			echo "Email sudah terdaftar";
		}else{
			$query = $this->db->query("INSERT INTO admin VALUES ('','".$email."','".$password."','".$name."', '0' ,'".$position."','".$phone."')");
			redirect('admin_area/admin_list');
		}
	}

	function edit_user()
	{
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$company = $this->input->post('company');
		$position = $this->input->post('position');
		$id = $this->uri->segment(3);

		$array = array(
			'fname' => $fname,
			'lname' => $lname,
			'email' => $email,
			'phone' => $phone,
			'company' => $company,
			'position' => $position,
		);

		$this->db->set($array);
		$this->db->where('id', $id);
		$this->db->update('user');

		redirect('admin_area/user_list');
	}

	function edit_admin()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$position = $this->input->post('position');
		$id = $this->uri->segment(3);

		$array = array(
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'position' => $position,
		);

		$this->db->set($array);
		$this->db->where('id', $id);
		$this->db->update('admin');

		redirect('admin_area/admin_list');
	}

	function report_ads()
	{
		$data['page_title'] = 'Report Ads';
		$data['current'] = 'Report Ads';
		$this->load->view('backend/base/adminheader',$data);
		$this->load->view('backend/report_ads',$data);
		$this->load->view('backend/base/adminfooter');
	}

	function report_seo()
	{
		$data['page_title'] = 'Report SEO';
		$data['current'] = 'Report SEO';
		$this->load->view('backend/base/adminheader',$data);
		$this->load->view('backend/report_seo',$data);
		$this->load->view('backend/base/adminfooter');
	}

	function report_web()
	{
		$data['page_title'] = 'Report Website';
		$data['current'] = 'Report Website';
		$this->load->view('backend/base/adminheader',$data);
		$this->load->view('backend/report_web',$data);
		$this->load->view('backend/base/adminfooter');
	}

	function set_paid()
	{
		if ($this->uri->segment(3) == null) {
			redirect('admin_area');

		}elseif ($this->uri->segment(3) != null) {

			$invoicenum = $this->uri->segment(3);

			$query = $this->db->query("SELECT * FROM invoice WHERE invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {

				$data = array(
					'status' => "PAID",
				);

				$this->db->where('invoicenum', $invoicenum);
				$this->db->update('invoice', $data);

				if ($this->uri->segment(4) == "ads") {
					redirect('admin_area/report_ads/');
				}elseif ($this->uri->segment(4) == "web") {
					redirect('admin_area/report_web/');
				}elseif ($this->uri->segment(4) == "seo") {
					redirect('admin_area/report_seo/');
				}

			}else{
				redirect('admin_area');
			}
		}
	}

	function set_unpaid()
	{
		if ($this->uri->segment(3) == null) {
			redirect('admin_area');

		}elseif ($this->uri->segment(3) != null) {

			$invoicenum = $this->uri->segment(3);

			$query = $this->db->query("SELECT * FROM invoice WHERE invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {

				$data = array(
					'status' => "UNPAID",
				);

				$this->db->where('invoicenum', $invoicenum);
				$this->db->update('invoice', $data);

				if ($this->uri->segment(4) == "ads") {
					redirect('admin_area/report_ads/');
				}elseif ($this->uri->segment(4) == "web") {
					redirect('admin_area/report_web/');
				}elseif ($this->uri->segment(4) == "seo") {
					redirect('admin_area/report_seo/');
				}
				
			}else{
				redirect('admin_area');
			}
		}
	}

	function set_pending()
	{
		if ($this->uri->segment(3) == null) {
			redirect('admin_area');

		}elseif ($this->uri->segment(3) != null) {

			$invoicenum = $this->uri->segment(3);

			$query = $this->db->query("SELECT * FROM invoice WHERE invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {

				$data = array(
					'status' => "PENDING CONFIRMATION",
				);

				$this->db->where('invoicenum', $invoicenum);
				$this->db->update('invoice', $data);

				if ($this->uri->segment(4) == "ads") {
					redirect('admin_area/report_ads/');
				}elseif ($this->uri->segment(4) == "web") {
					redirect('admin_area/report_web/');
				}elseif ($this->uri->segment(4) == "seo") {
					redirect('admin_area/report_seo/');
				}
				
			}else{
				redirect('admin_area');
			}
		}
	}

	function invoice()
	{
		if ($this->uri->segment(3) == null) {
			redirect('admin_area');

		}elseif ($this->uri->segment(3) != null) {
			$invoicenum = $this->uri->segment(3);

			$where = $invoicenum;
			$query = $this->db->query("SELECT * FROM invoice where invoicenum = '".$invoicenum."'");
			$cek = $query->num_rows();

			if ($cek > 0) {
				$query = $this->db->query("SELECT * FROM invoice where invoicenum = '".$invoicenum."'");
				$row = $query->row(1);

				$qseo = $this->db->query("SELECT * FROM services_seo where invoicenum = '".$invoicenum."'");
				$seo = $qseo->row(1);

				$qweb = $this->db->query("SELECT * FROM services_website where invoicenum = '".$invoicenum."'");
				$web = $qweb->row(1);

				$qads = $this->db->query("SELECT * FROM services_ads where invoicenum = '".$invoicenum."'");
				$ads = $qads->row(1);

				if ($row->servicesname == "SEO") {
					$data = array(
						'page_title' => 'Invoice #'.$invoicenum,
						'invoicenum' => $row->invoicenum,
						'status' => $row->status,
						'date' => $row->date,
						'method' => $row->method,
						'services' => $row->servicesname,
						'amount' => $row->amount,
						'seoperweek' => $seo->seoperweek,
						'seoduration' => $seo->seoduration,
					);
				}

				if ($row->servicesname == "Website") {
					$data = array(
						'page_title' => 'Invoice #'.$invoicenum,
						'invoicenum' => $row->invoicenum,
						'status' => $row->status,
						'date' => $row->date,
						'method' => $row->method,
						'services' => $row->servicesname,
						'amount' => $row->amount,
						'webtype' => $web->webtype,
						'webdomain' => $web->webdomain,
					);
				}

				if ($row->servicesname == "Ads") {
					$data = array(
						'page_title' => 'Invoice #'.$invoicenum,
						'invoicenum' => $row->invoicenum,
						'status' => $row->status,
						'date' => $row->date,
						'method' => $row->method,
						'services' => $row->servicesname,
						'amount' => $row->amount,
						'adstype' => $ads->adstype,
						'adsplatform' => $ads->adsplatform,
						'adsbudget' => $ads->budget,
						'adsduration' => $ads->duration,
					);
				}

				$this->load->view('backend/base/adminheader',$data);
				$this->load->view('backend/admininvoice',$data);
				$this->load->view('backend/base/adminfooter');			

			}else{
				redirect('admin_area');
			}
		}
	}

}
