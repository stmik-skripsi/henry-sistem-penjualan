<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$data = array('page_title' => '');
		$this->load->view('frontend/base/header2',$data);
		$this->load->view('frontend/home');
		$this->load->view('frontend/base/footer');
	}

	function contact()
	{
		$data = array('page_title' => 'Contact','body' => 'contact-page');
		$this->load->view('frontend/base/header',$data);
		$this->load->view('frontend/contact');
		$this->load->view('frontend/base/footer');
	}

	function blog()
	{
		$data = array('page_title' => 'Blog','body' => 'blog-version-1');
		$this->load->view('frontend/base/header',$data);
		$this->load->view('frontend/blog');
		$this->load->view('frontend/base/footer');
	}

	function about()
	{
		$data = array('page_title' => 'About');
		$this->load->view('frontend/base/header',$data);
		$this->load->view('frontend/about');
		$this->load->view('frontend/base/footer');
	}

	function portfolio()
	{
		$data = array('page_title' => 'Portfolio');
		$this->load->view('frontend/base/header',$data);
		$this->load->view('frontend/portfolio');
		$this->load->view('frontend/base/footer');
	}

	function pricing()
	{
		$data = array('page_title' => 'Pricing');
		$this->load->view('frontend/base/header',$data);
		$this->load->view('frontend/pricing');
		$this->load->view('frontend/base/footer');
	}
}
