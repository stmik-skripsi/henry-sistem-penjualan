    <!-- ++++ banner ++++ -->
    <section class="banner  o-hidden banner-inner pricing-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Pricing Plans</h1>
                <p class="semi-bold">Based on market demand, we have created 3 packages that will cover all
                    <br> your business needs .</p>
                <a href="#pricing-plans" class="medium-btn2 btn btn-nofill page-scroll">SEE OUR PRICING</a>
            </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <section id="pricing-plans" class="pricing-plans">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">For Businesses of All Sizes</h2>
            <hr class="dark-line" />
            <p class="regular-text">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
            </p>
            <!--end section title -->
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <ul class="pricing">
                        <li class="price">
                            <i class="flaticon-vegetable-shop"></i> Small Business
                            <span>
                                <sup>$</sup>4,999</span>
                        </li>
                        <li>10 Pages
                            <strong>Responsive Website</strong>
                        </li>
                        <li>5
                            <strong>PPC</strong> Campaigns</li>
                        <li>10
                            <strong>SEO</strong> Keywords</li>
                        <li>5
                            <strong>Facebook</strong> Camplaigns</li>
                        <li>2
                            <strong>Video</strong> Camplaigns</li>
                    </ul>
                    <!-- End of .pricing -->
                    <a href="" class="btn btn-fill">KICKSTART YOUR BUSINESS</a>
                </div>
                <!-- End of .col-sm-6 -->
                <div class="col-sm-6 col-md-4">
                    <ul class="pricing">
                        <li class="price">
                            <i class="flaticon-shop"></i> Medium Business
                            <span>
                                <sup>$</sup>9,999</span>
                        </li>
                        <li>20 Pages
                            <strong>Responsive Website</strong>
                        </li>
                        <li>10
                            <strong>PPC</strong> Campaigns</li>
                        <li>25
                            <strong>SEO</strong> Keywords</li>
                        <li>10
                            <strong>Facebook</strong> Camplaigns</li>
                        <li>5
                            <strong>Video</strong> Camplaigns</li>
                    </ul>
                    <!-- End of .pricing -->
                    <a href="" class="btn btn-fill">KICKSTART YOUR BUSINESS</a>
                </div>
                <!-- End of .col-sm-6 -->
                <div class="col-sm-6 col-md-4">
                    <ul class="pricing">
                        <li class="price">
                            <i class="flaticon-building-4"></i> Corporate Business
                            <span>Call</span>
                        </li>
                        <li>Unlimited Pages
                            <strong>Responsive Website</strong>
                        </li>
                        <li>Unlimited
                            <strong>PPC</strong> Campaigns</li>
                        <li>Unlimited
                            <strong>SEO</strong> Keywords</li>
                        <li>Unlimited
                            <strong>Facebook</strong> Camplaigns</li>
                        <li>Unlimited
                            <strong>Video</strong> Camplaigns</li>
                    </ul>
                    <!-- End of .pricing -->
                    <a href="" class="btn btn-fill">KICKSTART YOUR BUSINESS</a>
                </div>
                <!-- End of .col-sm-6 -->
            </div>
            <!-- End of .row -->
        </div>
        <!-- End of .container -->
    </section>
    <!-- End of .pricing-plans -->
