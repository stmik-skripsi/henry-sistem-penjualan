        <!-- ++++ footer ++++ -->
        <footer id="footer" class="no-margin">
            <!--footer content-->
            <div class="light-ash-bg">
                <div class="container">
                    <ul>
                        <li>
                            <!-- footer left content-->
                            <ul>
                                <li>
                                    <a href="index.html">
                                        <img src="<?php echo site_url(); ?>/assets/web/images/small-logo.png" alt="logo" class="img-responsive logo">
                                    </a>
                                </li>
                                <li>
                                    <p class="extra-small-text">&copy; 2020</p>
                                </li>
                                <li>
                                    <p class="extra-small-text">Your Company LLC.</p>
                                </li>
                            </ul>
                            <!--end footer left content-->
                        </li>
                        <li>
                            <!--footer service list-->
                            <ul>
                                <li>
                                    <a class="regular-text text-color-light">Services</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-design" class="extra-small-text">Design</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-development" class="extra-small-text">Development</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-online-marketing" class="extra-small-text">online marketing</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-business" class="extra-small-text">Business</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-technology" class="extra-small-text">Technology</a>
                                </li>
                            </ul>
                            <!--end footer service list-->
                        </li>
                        <li>
                            <!--footer Resources list-->
                            <ul>
                                <li>
                                    <a class="regular-text text-color-light">Resources</a>
                                </li>
                                <li>
                                    <a href="portfolio.html" class="extra-small-text">Portfolio</a>
                                </li>
                                <li>
                                    <a href="case-studies.html" class="extra-small-text">Case Studies</a>
                                </li>
                                <li>
                                    <a href="blog.html" class="extra-small-text">Blog</a>
                                </li>
                            </ul>
                            <!--end footer Resources list-->
                        </li>
                        <li>
                            <!--footer Support list-->
                            <ul>
                                <li>
                                    <a class="regular-text text-color-light">Support</a>
                                </li>
                                <li>
                                    <a href="contact.html" class="extra-small-text">Contact</a>
                                </li>
                                <li>
                                    <a href="privacy-policy.html" class="extra-small-text">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="terms-conditions.html" class="extra-small-text">Terms &amp; Conditions</a>
                                </li>
                            </ul>
                            <!--end footer Support list-->
                        </li>
                        <li class="big-width">
                            <!--footer social media list-->
                            <ul class="list-inline">
                                <li>
                                    <p class="regular-text text-color-light">Get in Touch</p>
                                    <ul class="social-links">
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-instagram"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-pinterest"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-linkedin"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <!--end footer social media list-->
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
        <!-- ++++ end footer ++++ -->
        <!--get a quote modal-->
        <div class="modal fade verticl-center-modal" id="getAQuoteModal" tabindex="-1" role="dialog" aria-labelledby="getAQuoteModal">
            <div class="modal-dialog getguoteModal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="icon-cross-circle"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="customise-form">
                                    <form class="email_form" method="post">
                                        <h3>Get a Free Quote</h3>
                                        <div class="form-group customised-formgroup">
                                            <span class="icon-user"></span>
                                            <input type="text" name="full_name" class="form-control" placeholder="Name">
                                        </div>
                                        <div class="form-group customised-formgroup">
                                            <span class="icon-envelope"></span>
                                            <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="form-group customised-formgroup">
                                            <span class="icon-telephone"></span>
                                            <input type="text" name="phone" class="form-control" placeholder="Phone">
                                        </div>
                                        <div class="form-group customised-formgroup">
                                            <span class="icon-laptop"></span>
                                            <input type="text" name="website" class="form-control" placeholder="Website">
                                        </div>
                                        <div class="form-group customised-formgroup">
                                            <span class="icon-bubble"></span>
                                            <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-fill full-width">GET A QUOTE
                                                <span class="icon-chevron-right"></span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3>What’s Next?</h3>
                                <ul class="list-with-arrow">
                                    <li>An email and phone call from one of our representatives.</li>
                                    <li>A time & cost estimation.</li>
                                    <li>An in-person meeting.</li>
                                </ul>
                                <div class="contact-info-box-wrapper">
                                    <div class="contact-info-box">
                                        <span class="icon-telephone"></span>
                                        <div>
                                            <h6>Give us a call</h6>
                                            <p>(123) 456 7890</p>
                                        </div>
                                    </div>
                                    <div class="contact-info-box">
                                        <span class="icon-envelope"></span>
                                        <div>
                                            <h6>Send an email</h6>
                                            <p>yourcompany@sample.com</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end get a quote modal-->
        <!-- ++++ Javascript libraries ++++ -->
        <!--js library of jQuery-->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/jquery.min.js"></script>
        <!--custom js-->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/script.js"></script>
        <!--js library of bootstrap-->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/bootstrap.min.js"></script>
        <!--js library for number counter-->
        <script src="<?php echo site_url(); ?>/assets/web/js/waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/jquery.counterup.min.js"></script>
        <!--js library for video popup-->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/jquery.magnific-popup.min.js"></script>
        <!-- js library for owl carousel -->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/owl.carousel.min.js"></script>
        <!--modernizer library-->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/modernizr.js"></script>
        <!-- Isotope script -->
        <script type="text/javascript" src="<?php echo site_url(); ?>/assets/web/js/isotope.min.js"></script>
        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script type="text/javascript" src="revolution/<?php echo site_url(); ?>/assets/web/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="revolution/<?php echo site_url(); ?>/assets/web/js/jquery.themepunch.revolution.min.js"></script>

        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    </body>
</html>