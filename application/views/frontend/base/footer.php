        <!-- ++++ footer ++++ -->
        <footer id="footer">
            <!--scroll to top-->
            <a class="top-btn page-scroll" href="#top">
                <span class="icon-chevron-up b-clor regular-text text-center"></span>
            </a>
            <!--end scroll to top-->
            <!--newsletter section-->
            <div class="grey-dark-bg text-center">
                <div class="container">
                    <h2>Sign up for our newsletter to stay up to date with tech news!</h2>
                    <div class="customise-form">
                        <form>
                            <div class="row">
                                <!--box one-->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group customised-formgroup">
                                        <span class="icon-user"></span>
                                        <input class="form-control" placeholder="Name">
                                    </div>
                                </div>
                                <!--end box one-->
                                <!--box two-->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group customised-formgroup">
                                        <span class="icon-envelope"></span>
                                        <input class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <!--end box two-->
                                <!--box three-->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div>
                                        <input class="btn btn-fill full-width" type="submit" value="SIGN UP FOR FREE!" />
                                    </div>
                                </div>
                                <!--end box three-->
                            </div>
                        </form>
                        <p class="light-gray-font-color">Lorem ipsum dolor sit amet, consectetuer adipi scing elit, sed diam nonummy nibh euismod tincidunt ut
                        laoreet.</p>
                    </div>
                </div>
            </div>
            <!--end newsletter section-->
            <!--footer content-->
            <div class="light-ash-bg">
                <div class="container">
                    <ul>
                        <li>
                            <!-- footer left content-->
                            <ul>
                                <li>
                                    <a href="index.html">
                                        <img src="<?php echo site_url(); ?>/assets/web/images/small-logo.png" alt="logo" class="img-responsive logo">
                                    </a>
                                </li>
                                <li>
                                    <p class="extra-small-text">&copy; 2017</p>
                                </li>
                                <li>
                                    <p class="extra-small-text">Your Company LLC.</p>
                                </li>
                            </ul>
                            <!--end footer left content-->
                        </li>
                        <li>
                            <!--footer service list-->
                            <ul>
                                <li>
                                    <a class="regular-text text-color-light">Services</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-design" class="extra-small-text">Design</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-development" class="extra-small-text">Development</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-online-marketing" class="extra-small-text">online marketing</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-business" class="extra-small-text">Business</a>
                                </li>
                                <li>
                                    <a href="services.html#bm-technology" class="extra-small-text">Technology</a>
                                </li>
                            </ul>
                            <!--end footer service list-->
                        </li>
                        <li>
                            <!--footer Resources list-->
                            <ul>
                                <li>
                                    <a class="regular-text text-color-light">Resources</a>
                                </li>
                                <li>
                                    <a href="portfolio.html" class="extra-small-text">Portfolio</a>
                                </li>
                                <li>
                                    <a href="case-studies.html" class="extra-small-text">Case Studies</a>
                                </li>
                                <li>
                                    <a href="blog.html" class="extra-small-text">Blog</a>
                                </li>
                            </ul>
                            <!--end footer Resources list-->
                        </li>
                        <li>
                            <!--footer Support list-->
                            <ul>
                                <li>
                                    <a class="regular-text text-color-light">Support</a>
                                </li>
                                <li>
                                    <a href="contact.html" class="extra-small-text">Contact</a>
                                </li>
                                <li>
                                    <a href="privacy-policy.html" class="extra-small-text">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="terms-conditions.html" class="extra-small-text">Terms &amp; Conditions</a>
                                </li>
                            </ul>
                            <!--end footer Support list-->
                        </li>
                        <li class="big-width">
                            <!--footer social media list-->
                            <ul class="list-inline">
                                <li>
                                    <p class="regular-text text-color-light">Get in Touch</p>
                                    <ul class="social-links">
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-facebook"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-instagram"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-pinterest"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="fa fa-linkedin"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <!--end footer social media list-->
                        </li>
                    </ul>
                </div>
            </div>
            <!--end footer content-->
        </footer>
        <!--end footer-->

        <!-- ++++ Javascript libraries ++++ -->
        <!--js library of jQuery-->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/jquery.min.js"></script>
        <!--js library of bootstrap-->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/bootstrap.min.js"></script>
        <!--js library for video popup-->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/jquery.magnific-popup.min.js"></script>
        <!-- js library for owl carousel -->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/owl.carousel.min.js"></script>
        <!--modernizer library-->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/isotope.min.js">
        </script>
        <!-- Slider Revolution core JavaScript files -->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- Slider Revolution extension scripts. Remove these scripts before uploading to server -->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <!--custom js-->
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/script.js"></script>

        <!--js library for number counter-->
        <script src="<?php echo site_url(); ?>assets/web/js/waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo site_url(); ?>assets/web/js/jquery.counterup.min.js"></script>

        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    </body>
</html>