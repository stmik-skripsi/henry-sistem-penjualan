<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php if (!empty($page_title)) {echo $page_title." - Digital Marketing";} else {echo "Digital Marketing";} ?></title>
    <!-- description -->
    <meta name="description" content="">
    <!-- keywords -->
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <!-- ++++ favicon ++++ -->
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>assets/web/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo site_url(); ?>assets/web/icon/favicon-96x96.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo site_url(); ?>assets/web/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/web/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo site_url(); ?>assets/web/icon/apple-touch-icon-114x114.png">
    <!-- ++++ bootstrap ++++ -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/bootstrap.min.css" />
    <!-- ++++ owl carousel ++++ -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/owl.theme.default.min.css" type="text/css">
    <!-- ++++ magnific-popup  ++++ -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/magnific-popup.css" />
    <!-- ++++ font Icon ++++ -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/fonts.css" />
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/web/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/web/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>assets/web/revolution/css/navigation.css">
    <!-- ++++ style ++++ -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/style.css" />
    <!-- responsive css -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/web/css/responsive.css" />
    <!-- [if IE]> <script src="<?php echo site_url(); ?>assets/web/js/html5shiv.js"></script> <![endif]  -->
</head>
<body class="<?php if (!empty($body)) {echo $body;} else {echo "";} ?>">
    <div class="header-wrapper">
        <!-- most top information -->
        <header id="top">
            <div class="container">
                <div class="form-element hidden-xs pull-left">
                    <form action="#" method="get" class="form-inline">
                        <input type="text" name="search" class="form-control" placeholder="Search">
                        <button type="submit" class="search-btn">
                            <i class="icon-magnifier"></i>
                        </button>
                    </form>
                </div>
                <!-- End of .form-element -->
                <div class="contact-info clearfix">
                    <ul class="pull-right list-inline">
                        <li></li>
                        <li>
                            <a href="<?php echo site_url(); ?>client_area/"><i class="icon-lock"></i>Client Area</a>
                        </li>
                    </ul>
                </div>
                <!-- End of .contact-info -->
            </div>
        </header>
        <!-- end most top information -->
        <!--navigation-->
        <div class="container no-padding">
            <nav id="navbar-main" class="navbar main-menu">
                <!--Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url(); ?>">
                        <img src="<?php echo site_url(); ?>assets/web/images/logo.png" alt="Brand" class="img-responsive" />
                    </a>
                </div>
                <!--Collect the nav links, and other content for toggling-->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="<?php if ($this->uri->segment(2) == 'about') { echo 'active';} ?>">
                            <a href="<?php echo site_url(); ?>page/about">About</a>
                        </li>
                        <li class="dropdown not-relative">
                            <a href="services.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services </a>
                            <div class="dropdown-menu dropdown-megamenu">
                                <ul class="megamenu">
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Design</li>
                                            <li>
                                                <a href="logo-and-branding.html"><i class="icon-palette"></i> Logo &amp; Branding</a>
                                            </li>
                                            <li>
                                                <a href="website-design.html"><i class="icon-laptop-phone"></i> Website Design</a>
                                            </li>
                                            <li>
                                                <a href="mobile-app-design.html"><i class="icon-phone"></i> Mobile App Design</a>
                                            </li>
                                            <li>
                                                <a href="graphic-design.html"><i class="icon-vector"></i> Graphic/Print Design</a>
                                            </li>
                                            <li>
                                                <a href="video-production.html"><i class="icon-camera"></i> Video Production</a>
                                            </li>
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Development</li>
                                            <li>
                                                <a href="content-management-system.html"><i class="icon-papers"></i> Content Management System</a>
                                            </li>
                                            <li>
                                                <a href="mobile-app-development.html"><i class="icon-smartphone-embed"></i> Mobile App Development</a>
                                            </li>
                                            <li>
                                                <a href="ecommerce.html"><i class="icon-cart"></i> eCommerce</a>
                                            </li>
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Online Marketing</li>
                                            <li>
                                                <a href="search-engine-optimization.html"><i class="icon-magnifier"></i> Search Engine Optimization</a>
                                            </li>
                                            <li>
                                                <a href="pay-per-click.html"><i class="icon-select2"></i> Pay-Per-Click</a>
                                            </li>
                                            <li>
                                                <a href="email-marketing.html"><i class="icon-envelope-open"></i> Email Marketing</a>
                                            </li>
                                            <li>
                                                <a href="display-marketing.html"><i class="icon-news"></i> Display Marketing</a>
                                            </li>
                                            <li>
                                                <a href="social-media-marketing.html"><i class="icon-share"></i> Social Media Marketing</a>
                                            </li>
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Business</li>
                                            <li>
                                                <a href="digital-strategy.html"><i class="icon-chart-settings"></i> Digital Strategy</a>
                                            </li>
                                            <li>
                                                <a href="business-consulting.html"><i class="icon-bubble-user"></i> Business Consulting</a>
                                            </li>
                                            <li>
                                                <a href="content-writing.html"><i class="icon-register"></i> Content Writing</a>
                                            </li>
                                            <li>
                                                <a href="reporting.html"><i class="icon-chart-growth"></i> Reporting</a>
                                            </li>
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                    <li>
                                        <ul class="dropdown-submenu clearfix">
                                            <li class="submenu-heading">Technology</li>
                                            <li>
                                                <a href="domain.html"><i class="icon-magnifier"></i> Domain</a>
                                            </li>
                                            <li>
                                                <a href="hosting.html"><i class="icon-server"></i> Hosting</a>
                                            </li>
                                            <li>
                                                <a href="big-data-analysis.html"><i class="icon-pie-chart"></i> Big Data Analysis</a>
                                            </li>
                                        </ul>
                                        <!-- End of .dropdown-submenu -->
                                    </li>
                                </ul>
                            </div>
                            <!-- End of .dropdown-menu -->
                        </li>
                        <li class="<?php if ($this->uri->segment(2) == 'pricing') { echo 'active';} ?>">
                            <a href="<?php echo site_url(); ?>page/pricing">Pricing</a>
                        </li>
                        <li class="<?php if ($this->uri->segment(2) == 'portfolio') { echo 'active';} ?>">
                            <a href="<?php echo site_url(); ?>page/portfolio">Portfolio</a>
                        </li>
                        <li class="<?php if ($this->uri->segment(2) == 'blog') { echo 'active';} ?>">
                            <a href="<?php echo site_url(); ?>page/blog">Blog</a>
                        </li>
                        <li class="<?php if ($this->uri->segment(2) == 'contact') { echo 'active';} ?>">
                            <a href="<?php echo site_url(); ?>page/contact">Contact</a>
                        </li>
                        <li class="menu-btn" data-toggle="modal" data-target="#getAQuoteModal">
                            <a class="btn btn-fill" href="#">GET A QUOTE
                                <span class="icon-chevron-right"></span>
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
            <!--end navigation-->
        </div>
    </div>
    <!-- End of .header-wrapper -->