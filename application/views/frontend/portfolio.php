    <!-- ++++ banner ++++ -->
    <section class="banner  o-hidden banner-inner portfolio-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Portfolio</h1>
                <p class="semi-bold">We use strategic approaches to provide our clients with high-end.
                    <br /> services that ensure superior customer satisfaction.</p>
                <a href="#more-portfolio" class="medium-btn2 btn btn-nofill page-scroll">DISCOVER MORE</a>
            </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <!-- ++++ Featured Project content ++++ -->
    <section class="o-hidden bg-white featured-design-section">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Featured Designs</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row margin-top-40 portfolio-p l-margin portfolio">
                <div class="col-md-4 col-sm-6">
                    <div class="grid-item">
                        <div class="img_container is-featured">
                            <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-img-one.jpg" alt="port_img" class="img-responsive">
                            <div class="overlay">
                                <a class="btn btn-nofill proDetModal1">Discover</a>
                            </div>
                            <!-- End of .overlay -->
                        </div>
                        <!-- End of .img_container -->
                        <div class="text-content">
                            <h3>
                                <a class="proDetModal proDetModal1">Ecommerce App Design
                                    <span>Mobile App</span>
                                </a>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="grid-item">
                        <div class="img_container is-featured">
                            <img src="<?php echo site_url(); ?>assets/web/images/portfolio/por-fea-project.jpg" alt="port_img" class="img-responsive">
                            <div class="overlay">
                                <a class="btn btn-nofill proDetModal2">Discover</a>
                            </div>
                            <!-- End of .overlay -->
                        </div>
                        <!-- End of .img_container -->
                        <div class="text-content">
                            <h3>
                                <a class="proDetModal proDetModal2">Logo Design
                                    <span>Website</span>
                                </a>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="grid-item">
                        <div class="img_container is-featured">
                            <img src="<?php echo site_url(); ?>assets/web/images/portfolio/por-fea-project-three.jpg" alt="port_img" class="img-responsive">
                            <div class="overlay">
                                <a class="btn btn-nofill proDetModal3">Discover</a>
                            </div>
                            <!-- End of .overlay -->
                        </div>
                        <!-- End of .img_container -->
                        <div class="text-content">
                            <h3>
                                <a class="proDetModal proDetModal3">Facebook Cover Design
                                    <span>Website</span>
                                </a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end Featured Project content ++++ -->
    <!-- ++++ full portfolio section ++++ -->
    <section class="portfolio-full portfolio clearfix" id="more-portfolio">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Full Portfolio</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="button-group filter-button-group clearfix">
                <button class="button is-checked" data-filter="*">All Work</button>
                <button class="button" data-filter=".a1">Website</button>
                <button class="button" data-filter=".a2">Logo</button>
                <button class="button" data-filter=".a3">Mobile App</button>
                <button class="button" data-filter=".a4">Social Media</button>
            </div>
            <!-- button-group ends -->
            <div class="grid clearfix row">
                <!-- Items for portfolio 
                currently using 12 items. More Items can be added. -->
                <div class="a2 a1 grid-item">
                    <div class="img_container">
                        <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-one.jpg" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal1">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal5">Diet Hospital
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                <div class="a3 grid-item">
                    <div class="img_container">
                        <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-two.jpg" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal2">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal2">Jet Airplane
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                <div class="a2 a1 grid-item">
                    <div class="img_container">
                        <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-three.jpg" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal3">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal3">Spring Water Service
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                <div class="a3 a4 grid-item">
                    <div class="img_container">
                        <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-four.jpg" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal4">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal4">Second Language
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                <div class="a2 a1 grid-item">
                    <div class="img_container">
                        <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-five.jpg" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal5">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal5">Home Buy and Sell
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                <div class="a3 a4 a1 grid-item">
                    <div class="img_container">
                        <img src="<?php echo site_url(); ?>assets/web/images/portfolio/portfolio-six.jpg" alt="port_img" class="img-responsive">
                        <div class="overlay">
                            <a class="btn btn-nofill proDetModal6">Discover</a>
                        </div>
                        <!-- End of .overlay -->
                    </div>
                    <!-- End of .img_container -->
                    <div class="text-content">
                        <h3>
                            <a class="proDetModal proDetModal6">Technical University
                                <span>Website</span>
                            </a>
                        </h3>
                    </div>
                </div>
                <!-- End of .grid-item -->
                <!-- to add more items to the portfolio section copy and paste any
                of the items underneath the last item -->
            </div>
            <!-- End of .grid -->
        </div>
    </section>
    <!--  ++++ end full portfolio section ++++ -->
