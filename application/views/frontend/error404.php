    <!-- ++++ 404 error ++++ -->
    <div class="error-404">
        <div class="container">
            <div class="error_message text-center">
                <span>opps!</span> We can’t seem to find your lost treasure.
                <br> Maybe you would like to
                <a href="<?php echo site_url(); ?>" class="b-clor"> go back? </a>
                <img src="<?php echo site_url(); ?>/assets/web/images/car-falling.png" alt="car falling image" class="img-responsive">
            </div>
            <!-- End of .error_message -->
        </div>
        <!-- End of .container -->
    </div>
    <!-- End of .404-error -->
    <!-- ++++ contact-us-now ++++ -->
    <section class="contact-us-now grey-dark-bg">
        <div class="container text-center">
            <h3>Can’t find the thing you’re looking for? Let us help you!</h3>
            <a href="<?php echo site_url(); ?>page/contact" class="btn btn-fill">contact us now</a>
        </div>
        <!-- End of .container -->
    </section>
    <!-- End of .404-error -->