<?php
$email = $this->session->userdata('email');
$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
$user = $query->row();
$uid = $user->id;
?>  
<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $current; ?></li>
          </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1"><?php echo $current; ?></h4>
      </div>
    </div>

    <div class="row row-xs">
      <div class="col-md-6 col-xl-4 mg-t-10">
        <div class="card ht-100p">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h6 class="mg-b-0">Ads</h6>
          </div>
          <?php
          $this->db->select('*');
          $this->db->from('services_ads');
          $this->db->join('invoice', 'services_ads.invoicenum = invoice.invoicenum');
          $where = array('invoice.iduser' => $uid, 'status' => "PAID");
          $this->db->where($where);
          $query = $this->db->get();
          $no = 1; foreach ($query->result_array() as $q) { ?>
            <ul class="list-group list-group-flush tx-13">
              <li class="list-group-item d-flex pd-sm-x-20">
                <div class="avatar"><span class="avatar-initial rounded-circle bg-gray-600"><i data-feather="globe"></i></span></div>
                <div class="pd-l-10">
                  <p class="tx-medium mg-b-0"><?php echo $q['adsplatform']; ?></p>
                  <small class="tx-12 tx-color-03 mg-b-0">Invoice #<?php echo $q['invoicenum']; ?></small>
                </div>
                <div class="mg-l-auto d-flex align-self-center">
                  <nav class="nav nav-icon-only">
                    <a href="<?php echo site_url(); ?>client_area/services/<?php echo $q['invoicenum']; ?>" class="nav-link d-none d-sm-block"><i data-feather="settings"></i></a>

                  </nav>
                </div>
              </li>
            </ul>
          <?php } ?>
        </div><!-- card -->
      </div>
      <div class="col-md-6 col-xl-4 mg-t-10">
        <div class="card ht-100p">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h6 class="mg-b-0">Website</h6>
          </div>
          <ul class="list-group list-group-flush tx-13">
            <?php
            $this->db->select('*');
            $this->db->from('services_website');
            $this->db->join('invoice', 'services_website.invoicenum = invoice.invoicenum');
            $where = array('invoice.iduser' => $uid, 'status' => "PAID");
            $this->db->where($where);
            $query = $this->db->get();
            $no = 1; foreach ($query->result_array() as $q) { ?>
              <ul class="list-group list-group-flush tx-13">
                <li class="list-group-item d-flex pd-sm-x-20">
                  <div class="avatar"><span class="avatar-initial rounded-circle bg-gray-600"><i data-feather="globe"></i></span></div>
                  <div class="pd-l-10">
                    <p class="tx-medium mg-b-0"><?php echo $q['webdomain']; ?></p>
                    <small class="tx-12 tx-color-03 mg-b-0">Invoice #<?php echo $q['invoicenum']; ?></small>
                  </div>
                  <div class="mg-l-auto d-flex align-self-center">
                    <nav class="nav nav-icon-only">
                      <a href="<?php echo site_url(); ?>client_area/services/<?php echo $q['invoicenum']; ?>" class="nav-link d-none d-sm-block"><i data-feather="settings"></i></a>

                    </nav>
                  </div>
                </li>
              </ul>
            <?php } ?>
          </ul>
        </div><!-- card -->
      </div>
      <div class="col-md-6 col-xl-4 mg-t-10">
        <div class="card ht-100p">
          <div class="card-header d-flex align-items-center justify-content-between">
            <h6 class="mg-b-0">SEO</h6>
          </div>
          <ul class="list-group list-group-flush tx-13">
            <?php
            $this->db->select('*');
            $this->db->from('services_seo');
            $this->db->join('invoice', 'services_seo.invoicenum = invoice.invoicenum');
            $where = array('invoice.iduser' => $uid, 'status' => "PAID");
            $this->db->where($where);
            $query = $this->db->get();
            $no = 1; foreach ($query->result_array() as $q) { ?>
              <ul class="list-group list-group-flush tx-13">
                <li class="list-group-item d-flex pd-sm-x-20">
                  <div class="avatar"><span class="avatar-initial rounded-circle bg-gray-600"><i data-feather="globe"></i></span></div>
                  <div class="pd-l-10">
                    <p class="tx-medium mg-b-0"><?php echo $q['seowebsite']; ?></p>
                    <small class="tx-12 tx-color-03 mg-b-0">Invoice #<?php echo $q['invoicenum']; ?></small>
                  </div>
                  <div class="mg-l-auto d-flex align-self-center">
                    <nav class="nav nav-icon-only">
                      <a href="<?php echo site_url(); ?>client_area/services/<?php echo $q['invoicenum']; ?>" class="nav-link d-none d-sm-block"><i data-feather="settings"></i></a>
                    </nav>
                  </div>
                </li>
              </ul>
            <?php } ?>
          </ul>
        </div><!-- card -->
      </div>
    </div><!-- row -->
  </div><!-- container -->
    </div><!-- content -->