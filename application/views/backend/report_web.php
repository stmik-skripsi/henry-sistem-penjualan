  <div class="content-body">
    <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
      <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
        <div>
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-style1 mg-b-10">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?php echo $current; ?></li>
            </ol>
          </nav>
          <h4 class="mg-b-0 tx-spacing--1"><?php echo $current; ?></h4>
        </div>
      </div>

      <div class="row row-xs">
        <div class="col-12 mg-t-10">
          <div class="card ht-100p">
            <div class="card-header d-flex align-items-center justify-content-between">
              <h6 class="mg-b-0">Ads</h6>
            </div>
            <?php
            $this->db->select('*');
            $this->db->from('services_website');
            $this->db->join('invoice', 'services_website.invoicenum = invoice.invoicenum');
            $query = $this->db->get();
            $no = 1; foreach ($query->result_array() as $q) { ?>
              <ul class="list-group list-group-flush tx-13">
                <li class="list-group-item d-flex pd-sm-x-20">
                  <div class="avatar"><span class="avatar-initial rounded-circle bg-gray-600"><i data-feather="globe"></i></span></div>
                  <div class="pd-l-10">
                    <p class="tx-medium mg-b-0"><?php echo $q['webdomain']; ?></p>
                    <small class="tx-12 tx-color-03 mg-b-0">Invoice #<?php echo $q['invoicenum']; ?></small>
                  </div>
                  <div class="mg-l-auto d-flex align-self-center">
                    <nav class="nav nav-icon-only">
                      <span class="mg-r-10">Due : <?php $duedate = strtotime($q['date']); $duedate = strtotime("+7 day", $duedate); echo date('F d, Y', $duedate); ?></span>
                      <span class="mg-r-10">|</span>
                      <span class="mg-r-10">Status : </span>
                      <a href="<?php echo site_url(); ?>admin_area/set_paid/<?php echo $q['invoicenum']; ?>/web" class="btn btn-sm pd-x-15 btn-uppercase mg-r-10 <?php if ($q['status'] == "PAID") { echo "btn-success";}else{ echo "btn-outline-success";} ?>">PAID</a>
                      <a href="<?php echo site_url(); ?>admin_area/set_unpaid/<?php echo $q['invoicenum']; ?>/web" class="btn btn-sm pd-x-15 btn-uppercase mg-r-10 <?php if ($q['status'] == "UNPAID") { echo "btn-danger";}else{ echo "btn-outline-danger";} ?>">UNPAID</a>
                      <a href="<?php echo site_url(); ?>admin_area/set_pending/<?php echo $q['invoicenum']; ?>/web" class="btn btn-sm pd-x-15 btn-uppercase mg-r-10 <?php if ($q['status'] == "PENDING CONFIRMATION") { echo "btn-warning";}else{ echo "btn-outline-warning";} ?>">PENDING CONFIRMATION</a>
                      <a href="<?php echo site_url(); ?>admin_area/invoice/<?php echo $q['invoicenum']; ?>" class="nav-link d-none d-sm-block"><i data-feather="settings"></i></a>
                    </nav>
                  </div>
                </li>
              </ul>
            <?php } ?>
          </div><!-- card -->
        </div>
      </div>
    </div><!-- row -->
  </div><!-- container -->
    </div><!-- content -->