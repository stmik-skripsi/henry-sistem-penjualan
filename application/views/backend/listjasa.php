<div class="content content-fixed content-auth-alt">
  <div class="container ht-100p tx-center">
    <div class="row justify-content-center">
      <div class="col-10 col-sm-6 col-md-4 col-lg-3 d-flex flex-column">
        <div class="tx-100 lh-1"><i class="icon ion-ios-bicycle"></i></div>
        <h3 class="mg-b-25">Digital Ads</h3>
        <p class="tx-color-03 mg-b-50">Promote your content to users through various online and digital channels</p>
        <a class="btn btn-primary btn-block" href="<?php echo site_url(); ?>client_area/order_ads">Start Now</a>
      </div><!-- col -->
      <div class="col-10 col-sm-6 col-md-4 col-lg-3 mg-t-40 mg-sm-t-0 d-flex flex-column">
        <div class="tx-100 lh-1"><i class="icon ion-ios-car"></i></div>
        <h3 class="mg-b-25">Website</h3>
        <p class="tx-color-03 mg-b-30">Create a professional website with affordable prices. Choose what kind of website you want to create</p>
        <a class="btn btn-primary btn-block" href="<?php echo site_url(); ?>client_area/order_website">Start Now</a>
      </div><!-- col -->
      <div class="col-10 col-sm-6 col-md-4 col-lg-3 mg-t-40 mg-md-t-0 d-flex flex-column">
        <div class="tx-100 lh-1"><i class="icon ion-ios-boat"></i></div>
        <h3 class="mg-b-25">SEO</h3>
        <p class="tx-color-03 mg-b-50">Improve your search engine rankings with professional SEO experts </p>
        <a class="btn btn-primary btn-block" href="<?php echo site_url(); ?>client_area/order_seo">Start Now</a>
      </div><!-- col -->
    </div><!-- row -->
  </div><!-- container -->
    </div><!-- content -->