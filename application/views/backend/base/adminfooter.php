    <?php 
    $id = $this->uri->segment(3);
    $query = $this->db->query("SELECT * FROM user where id = '".$id."'");
    $user = $query->row(1);

    $aid = $this->uri->segment(3);
    $query = $this->db->query("SELECT * FROM admin where id = '".$aid."'");
    $admin = $query->row(1);
    ?>
    <div class="modal fade effect-scale" id="modalNewUser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="<?php echo site_url(); ?>admin_area/add_user" method="POST">
                    <div class="modal-body pd-20 pd-sm-30">
                        <button type="button" class="close pos-absolute t-15 r-20" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="tx-18 tx-sm-20 mg-b-40">Create New User</h5>
                        <div class="d-sm-flex">
                            <div class="mg-sm-r-30">
                                <div class="pos-relative d-inline-block mg-b-20">
                                    <div class="avatar avatar-xxl">
                                        <span class="avatar-initial rounded-circle bg-gray-700 tx-normal">
                                            <i class="icon ion-md-person"></i>
                                        </span>
                                    </div>
                                </div>
                            </div><!-- col -->
                            <div class="mg-sm-r-30 flex-fill">

                                <h6 class="mg-b-10">Personal Information</h6>
                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Firstname" name="fname">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Lastname" name="lname">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="password" class="form-control" placeholder="Passowrd" name="password">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Contact Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Phone number" name="phone">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="email" class="form-control" placeholder="Email address" name="email">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Company Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Company Name" name="company">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Position" name="position">
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="wd-100p d-flex flex-column flex-sm-row justify-content-end">
                            <button type="submit" class="btn btn-primary mg-b-5 mg-sm-b-0">Add User</button>
                            <button type="button" class="btn btn-secondary mg-sm-l-5" data-dismiss="modal">Discard</button>
                        </div>
                    </div><!-- modal-footer -->
                </form>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class="modal fade effect-scale" id="modalEditUser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="<?php echo site_url(); ?>admin_area/edit_user/<?php echo $aid; ?>" method="POST">
                    <div class="modal-body pd-20 pd-sm-30">
                        <button type="button" class="close pos-absolute t-15 r-20" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <h5 class="tx-18 tx-sm-20 mg-b-20">Edit User</h5>

                        <div class="d-sm-flex">
                            <div class="mg-sm-r-30">
                                <div class="pos-relative d-inline-block mg-b-20">
                                    <div class="avatar avatar-xxl">
                                        <span class="avatar-initial rounded-circle bg-gray-700 tx-normal">A</span>
                                    </div>
                                </div>
                            </div><!-- col -->
                            <div class="flex-fill">
                                <h6 class="mg-b-10">Personal Information</h6>
                                <div class="form-group mg-b-10">
                                    <input type="text" name="fname" class="form-control" placeholder="Firstname" value="<?php if ($id == null) {}else{echo $user->fname;} ?>">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="text" name="lname" class="form-control" placeholder="Lastname" value="<?php if ($id == null) {}else{echo $user->lname;} ?>">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Contact Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" name="phone" class="form-control" placeholder="Phone number" value="<?php if ($id == null) {}else{echo $user->phone;} ?>">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="email" name="email" class="form-control" placeholder="Email address" value="<?php if ($id == null) {}else{echo $user->email;} ?>">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Company Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Company Name" name="company" value="<?php if ($id == null) {}else{echo $user->company;} ?>">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Position" name="position" value="<?php if ($id == null) {}else{echo $user->position;} ?>">
                                </div><!-- form-group -->
                            </div><!-- col -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="wd-100p d-flex flex-column flex-sm-row justify-content-end">
                            <button type="submit" class="btn btn-primary mg-b-5 mg-sm-b-0">Save Changes</button>
                            <button type="button" class="btn btn-secondary mg-sm-l-5" data-dismiss="modal">Cancel</button>
                        </div>
                    </div><!-- modal-footer -->
                </form>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class="modal fade effect-scale" id="modalDeleteUser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Delete User</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mg-b-0">Do you really want to delete this user?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="location.href='<?php echo site_url(); ?>admin_area/delete_user/<?php echo $uid; ?>';">Continue Delete</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade effect-scale" id="modalNewAdmin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="<?php echo site_url(); ?>admin_area/add_admin" method="POST">
                    <div class="modal-body pd-20 pd-sm-40">
                        <button type="button" class="close pos-absolute t-15 r-20" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h5 class="tx-18 tx-sm-20 mg-b-20">Create New Admin</h5>
                        <div class="d-sm-flex">
                            <div class="mg-sm-r-30">
                                <div class="pos-relative d-inline-block mg-b-20">
                                    <div class="avatar avatar-xxl">
                                        <span class="avatar-initial rounded-circle bg-gray-700 tx-normal">
                                            <i class="icon ion-md-person"></i>
                                        </span>
                                    </div>
                                    <a href="" class="contact-edit-photo"><i data-feather="edit-2"></i></a>
                                </div>
                            </div><!-- col -->
                            <div class="flex-fill">
                                <h6 class="mg-b-10">Personal Information</h6>
                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Name" name="name">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="password" class="form-control" placeholder="Passowrd" name="password">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Contact Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Phone number" name="phone">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="email" class="form-control" placeholder="Email address" name="email">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Company Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Position" name="position">
                                </div><!-- form-group --> 
                            </div><!-- col -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="wd-100p d-flex flex-column flex-sm-row justify-content-end">
                            <button type="submit" class="btn btn-primary mg-b-5 mg-sm-b-0">Save Admin</button>
                            <button type="button" class="btn btn-secondary mg-sm-l-5" data-dismiss="modal">Discard</button>
                        </div>
                    </div><!-- modal-footer -->
                </form>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class="modal fade effect-scale" id="modalEditAdmin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="POST" action="<?php echo site_url(); ?>admin_area/edit_admin/<?php echo $aid; ?>">
                    <div class="modal-body pd-20 pd-sm-30">
                        <button type="button" class="close pos-absolute t-15 r-20" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <h5 class="tx-18 tx-sm-20 mg-b-20">Edit Admin</h5>
                        <div class="d-sm-flex">
                            <div class="mg-sm-r-30">
                                <div class="pos-relative d-inline-block mg-b-20">
                                    <div class="avatar avatar-xxl">
                                        <span class="avatar-initial rounded-circle bg-gray-700 tx-normal">A</span>
                                    </div>
                                </div>
                            </div><!-- col -->
                            <div class="flex-fill">
                                <h6 class="mg-b-10">Personal Information</h6>
                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Name" name="name" value="<?php if ($aid == null) {}else{echo $admin->name;} ?>">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Contact Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Phone number" name="phone" value="<?php if ($aid == null) {}else{echo $admin->phone;} ?>">
                                </div><!-- form-group -->
                                <div class="form-group mg-b-10">
                                    <input type="email" class="form-control" placeholder="Email address" name="email" value="<?php if ($aid == null) {}else{ echo $admin->email;} ?>">
                                </div><!-- form-group -->

                                <h6 class="mg-t-20 mg-b-10">Company Information</h6>

                                <div class="form-group mg-b-10">
                                    <input type="text" class="form-control" placeholder="Position" name="position" value="<?php if ($aid == null) {}else{echo $admin->position;} ?>">
                                </div><!-- form-group --> 
                            </div><!-- col -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="wd-100p d-flex flex-column flex-sm-row justify-content-end">
                            <button type="submit" class="btn btn-primary mg-b-5 mg-sm-b-0">Save Changes</button>
                            <button type="button" class="btn btn-secondary mg-sm-l-5" data-dismiss="modal">Cancel</button>
                        </div>
                    </div><!-- modal-footer -->
                </form>
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->

    <div class="modal fade effect-scale" id="modalDeleteAdmin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title">Delete Admin</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mg-b-0">Do you really want to delete this account?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="location.href='<?php echo site_url(); ?>admin_area/delete_admin/<?php echo $uid; ?>';">Continue Delete</button>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/feather-icons/feather.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.stack.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/chart.js/Chart.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/jquery.vmap.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/maps/jquery.vmap.usa.js"></script>

    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.aside.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.sampledata.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashboard-one.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashboard-two.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.contacts.js"></script>

    <!-- append theme customizer -->
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/js-cookie/js.cookie.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.settings.js"></script>
</body>
</html>
