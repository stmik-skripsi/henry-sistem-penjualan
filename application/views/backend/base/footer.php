	</div>

	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery/jquery.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/feather-icons/feather.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.stack.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.resize.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/chart.js/Chart.bundle.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/maps/jquery.vmap.usa.js"></script>

	<script src="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/prism.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/parsleyjs/parsley.min.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery-steps/build/jquery.steps.min.js"></script>

	<script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.sampledata.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/js/dashboard-one.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/js/dashboard-two.js"></script>


	<!-- append theme customizer -->
	<script src="<?php echo site_url(); ?>assets/dashboard/lib/js-cookie/js.cookie.js"></script>
	<script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.settings.js"></script>

	<?php if ($this->session->userdata('status')!="loggedin") { echo "";} else { ?> 
		<!--Start of Tawk.to Script-->
		<script type="text/javascript">
			var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
			(function(){
				var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
				s1.async=true;
				s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
				s1.charset='UTF-8';
				s1.setAttribute('crossorigin','*');
				s0.parentNode.insertBefore(s1,s0);
			})();
		</script>
		<!--End of Tawk.to Script-->
	<?php } ?>
</body>
</html>
