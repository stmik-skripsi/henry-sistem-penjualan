<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Twitter -->
  <meta name="twitter:site" content="@themepixels">
  <meta name="twitter:creator" content="@themepixels">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="DashForge">
  <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
  <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

  <!-- Facebook -->
  <meta property="og:url" content="http://themepixels.me/dashforge">
  <meta property="og:title" content="DashForge">
  <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

  <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
  <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="600">

  <!-- Meta -->
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url(); ?>assets/dashboard/img/favicon.png">

  <title><?php if (!empty($page_title)) {echo $page_title." - Digital Marketing";} else {echo "Digital Marketing";} ?></title>

  <!-- vendor css -->
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/typicons.font/typicons.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/themes/prism-vs.css" rel="stylesheet">


  <!-- DashForge CSS -->
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.css">
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.auth.css">
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.demo.css">

  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/custom.css">

</head>
<body>
  <header class="navbar navbar-header">
    <div class="navbar-brand">
      <a href="<?php echo site_url(); ?>" class="df-logo">Digital<span>&nbsp;Marketer</span></a>
    </div><!-- navbar-brand -->
  </div><!-- navbar-menu-wrapper -->
</header><!-- navbar -->
