<?php
$email = $this->session->userdata('email');
$query = $this->db->query("SELECT * FROM admin where email = '".$email."'");
$admin = $query->row();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Twitter -->
  <meta name="twitter:site" content="@themepixels">
  <meta name="twitter:creator" content="@themepixels">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="DashForge">
  <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
  <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

  <!-- Facebook -->
  <meta property="og:url" content="http://themepixels.me/dashforge">
  <meta property="og:title" content="DashForge">
  <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

  <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
  <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="1200">
  <meta property="og:image:height" content="600">

  <!-- Meta -->
  <meta name="description" content="Responsive Bootstrap 4 Dashboard Template">
  <meta name="author" content="ThemePixels">

  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url(); ?>/assets/dashboard/img/favicon.png">
  <title><?php if (!empty($page_title)) {echo $page_title." - Admin Area";} else {echo "Admin Area";} ?></title>

  <!-- vendor css -->
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/jqvmap.min.css" rel="stylesheet">

  <!-- DashForge CSS -->
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.css">
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.dashboard.css">
  <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.contacts.css">
</head>
<body class="<?php if ($page_title == "User List" or $page_title == "Admin List") {echo "app-contact";} else {echo "";} ?>">
  <aside class="aside aside-fixed">
    <div class="aside-header">
      <a href="<?php echo site_url(); ?>admin_area" class="aside-logo">Digital<span>&nbsp;Marketer</span></a>
      <a href="<?php echo site_url(); ?>admin_area" class="aside-menu-link">
        <i data-feather="menu"></i>
        <i data-feather="x"></i>
      </a>
      <a href="" id="contactContentHide" class="burger-menu d-none"><i data-feather="arrow-left"></i></a>
    </div>
    <div class="aside-body">
      <div class="aside-loggedin">
        <div class="d-flex align-items-center justify-content-start">
          <div class="avatar avatar-sm avatar-online"><span class="avatar-initial rounded-circle bg-gray-700"><i data-feather="user" style="width: 30px; height: 30px"></i></span></div>
          <div class="aside-alert-link">
            <a href="<?php echo site_url(); ?>admin_area/logout" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
          </div>
        </div>
        <div class="aside-loggedin-user">
          <a href="" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
            <h6 class="tx-semibold mg-b-0"><?php echo $admin->name; ?></h6>
          </a>
          <p class="tx-color-03 tx-12 mg-b-0"><?php if ($admin->role == 1) {echo "Super Administrator";} else {echo "Administrator";} ?></p>
        </div>
      </div><!-- aside-loggedin -->
      <ul class="nav nav-aside">
        <li class="nav-label">Admin</li>
        <li class="nav-item with-sub">
          <a href="" class="nav-link"><i data-feather="user"></i> <span>User Account</span></a>
          <ul>
            <li><a href="<?php echo site_url(); ?>admin_area/user_list">User List</a></li>
            <li><a href="#modalNewUser" data-toggle="modal">Add New User</a></li>
          </ul>
        </li>
        <li class="nav-item with-sub">
          <a href="" class="nav-link"><i data-feather="user"></i> <span>Admin Account</span></a>
          <ul>
            <li><a href="<?php echo site_url(); ?>admin_area/admin_list">Admin List</a></li>
            <?php if ($admin->role == 1) { ?>
            <li><a href="#modalNewAdmin" data-toggle="modal">Add New Admin</a></li>
          <?php }else{} ?>
          </ul>
        </li>

        <li class="nav-label mg-t-25">Report</li>
        <li class="nav-item"><a href="<?php echo site_url(); ?>admin_area/report_ads" class="nav-link"><i data-feather="layers"></i> <span>Ads</span></a></li>
        <li class="nav-item"><a href="<?php echo site_url(); ?>admin_area/report_web" class="nav-link"><i data-feather="box"></i> <span>Website</span></a></li>
        <li class="nav-item"><a href="<?php echo site_url(); ?>admin_area/report_seo" class="nav-link"><i data-feather="bar-chart-2"></i> <span>SEO</span></a></li>

      </ul>
    </div>
  </aside>

  <div class="content ht-100v pd-0">
    <div class="content-header">
      <div class="content-search">
      </div>
      <nav class="nav">
        <a href="" class="nav-link"><i data-feather="help-circle"></i></a>
        <a href="" class="nav-link"><i data-feather="grid"></i></a>
        <a href="" class="nav-link"><i data-feather="align-left"></i></a>
      </nav>
    </div><!-- content-header -->