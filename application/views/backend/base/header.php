  <?php
  $email = $this->session->userdata('email');
  $query = $this->db->query("SELECT * FROM user where email = '".$email."'");
  $user = $query->row();
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="DashForge">
    <meta name="twitter:description" content="Responsive Bootstrap 4 Dashboard Template">
    <meta name="twitter:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/dashforge">
    <meta property="og:title" content="DashForge">
    <meta property="og:description" content="Responsive Bootstrap 4 Dashboard Template">

    <meta property="og:image" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/dashforge/img/dashforge-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url(); ?>/assets/dashboard/img/favicon.png">

    <title><?php if (!empty($page_title)) {echo $page_title." - Digital Marketing";} else {echo "Digital Marketing";} ?></title>

    <!-- vendor css -->
    <link href="<?php echo site_url(); ?>assets/dashboard/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/dashboard/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/jqvmap.min.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/dashboard/lib/typicons.font/typicons.css" rel="stylesheet">
    <link href="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/themes/prism-vs.css" rel="stylesheet">


    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.dashboard.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.auth.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.profile.css">
    <link rel="stylesheet" href="<?php echo site_url(); ?>assets/dashboard/css/dashforge.demo.css">
  </head>
  <body class="page-profile">

    <header class="navbar navbar-header navbar-header-fixed">
      <a href="" id="mainMenuOpen" class="burger-menu"><i data-feather="menu"></i></a>
      <div class="navbar-brand">
        <a href="<?php echo site_url(); ?>client_area" class="df-logo">Digital<span>&nbsp;Marketer</span></a>
      </div><!-- navbar-brand -->
      <div id="navbarMenu" class="navbar-menu-wrapper">
        <div class="navbar-menu-header">
          <a href="../../index.html" class="df-logo">Digital<span>&nbsp;Marketer</span></a>
          <a id="mainMenuClose" href=""><i data-feather="x"></i></a>
        </div><!-- navbar-menu-header -->
        <ul class="nav navbar-menu ">
          <li class="nav-item <?php echo ($page_title == 'Dashboard')?"active":""; ?>"><a href="<?php echo site_url(); ?>client_area/" class="nav-link"><i data-feather="archive"></i> Dashboard</a></li>
          <li class="nav-item <?php echo ($page_title == 'My Services')?"active":""; ?>"><a href="<?php echo site_url(); ?>client_area/services" class="nav-link"><i data-feather="archive"></i> My Services</a></li>
          <li class="nav-item <?php echo ($page_title == 'Order')?"active":""; ?>"><a href="<?php echo site_url(); ?>client_area/order" class="nav-link"><i data-feather="archive"></i> Order New Services</a></li>
          <li class="nav-item <?php echo ($page_title == 'Invoice List')?"active":""; ?>"><a href="<?php echo site_url(); ?>client_area/invoice" class="nav-link"><i data-feather="archive"></i> Invoices</a></li>
        </ul>
      </div><!-- navbar-menu-wrapper -->
      <div class="navbar-right">
        <div class="dropdown dropdown-profile">
          <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
            <div class="avatar avatar-sm"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
          </a><!-- dropdown-link -->
          <div class="dropdown-menu dropdown-menu-right tx-13">
            <div class="avatar avatar-lg mg-b-15"><img src="https://via.placeholder.com/500" class="rounded-circle" alt=""></div>
            <h6 class="tx-semibold mg-b-5"><?php echo "{$user->fname} {$user->lname}";?></h6>
            <p class="tx-12 tx-color-03">Customer</p>
            <!-- navbar-menu-wrapper 
            <a href="<?php echo site_url(); ?>client_area/profile_view" class="dropdown-item"><i data-feather="edit-3"></i> Edit Profile</a>
            <a href="<?php echo site_url(); ?>client_area/profile_view" class="dropdown-item"><i data-feather="user"></i> View Profile</a>-->
            <div class="dropdown-divider"></div>
            <a href="<?php echo site_url(); ?>client_area/help" class="dropdown-item"><i data-feather="help-circle"></i> Help Center</a>
            <a href="<?php echo site_url(); ?>client_area/logout" class="dropdown-item"><i data-feather="log-out"></i>Sign Out</a>
          </div><!-- dropdown-menu -->
        </div><!-- dropdown -->
      </div><!-- navbar-right -->
      <div class="navbar-search">
        <div class="navbar-search-header">
          <input type="search" class="form-control" placeholder="Type and hit enter to search...">
          <button class="btn"><i data-feather="search"></i></button>
          <a id="navbarSearchClose" href="" class="link-03 mg-l-5 mg-lg-l-10"><i data-feather="x"></i></a>
        </div><!-- navbar-search-header -->
        <div class="navbar-search-body">
          <label class="tx-10 tx-medium tx-uppercase tx-spacing-1 tx-color-03 mg-b-10 d-flex align-items-center">Recent Searches</label>
          <ul class="list-unstyled">
            <li><a href="dashboard-one.html">modern dashboard</a></li>
            <li><a href="app-calendar.html">calendar app</a></li>
            <li><a href="../../collections/modal.html">modal examples</a></li>
            <li><a href="../../components/el-avatar.html">avatar</a></li>
          </ul>

          <hr class="mg-y-30 bd-0">

          <label class="tx-10 tx-medium tx-uppercase tx-spacing-1 tx-color-03 mg-b-10 d-flex align-items-center">Search Suggestions</label>

          <ul class="list-unstyled">
            <li><a href="dashboard-one.html">cryptocurrency</a></li>
            <li><a href="app-calendar.html">button groups</a></li>
            <li><a href="../../collections/modal.html">form elements</a></li>
            <li><a href="../../components/el-avatar.html">contact app</a></li>
          </ul>
        </div><!-- navbar-search-body -->
      </div><!-- navbar-search -->
    </header><!-- navbar -->
