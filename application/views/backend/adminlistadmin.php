<?php
$email = $this->session->userdata('email');
$query = $this->db->query("SELECT * FROM admin where email = '".$email."'");
$admin = $query->row();
?>
<div class="content-body pd-0">
  <div class="contact-wrapper contact-wrapper-two">
    <div class="contact-sidebar" style="left: 0px; width: 340px">
      <div class="contact-sidebar-body" style="top: 0px">
        <div class="tab-content">
          <div id="tabContact" class="tab-pane fade active show">
            <div class="pd-y-20 pd-x-10 contact-list">
              <?php
              $query = $this->db->query('SELECT * FROM admin');
              $no = 1; foreach ($query->result_array() as $q) { ?>
                <div class="media <?php if ( $uid == $q['id']) {echo "active";} ?>" onclick="location.href='<?php echo site_url(); ?>admin_area/admin_list/<?php echo $q['id']; ?>';">
                  <div class="avatar avatar-sm avatar-online"><span class="avatar-initial rounded-circle bg-gray-700"><i data-feather="user"></i></span></div>
                  <div class="media-body mg-l-10">
                    <h6 class="tx-13 mg-b-3"><?php echo $q['name']; ?></h6>
                    <span class="tx-12"><?php if ($q['role'] == 1) {echo "Super Administrator";} else { echo "Administrator";} ?></span>
                  </div><!-- media-body -->
                  <nav>
                    <a href="<?php echo site_url(); ?>admin_area/admin_list/<?php echo $q['id']; ?>" data-toggle="tooltip" title="View Data"><i data-feather="eye"></i></a>
                  </nav>
                </div><!-- media -->
              <?php } ?>
            </div><!-- contact-list -->
          </div><!-- tab-pane -->
        </div><!-- tab-content -->
      </div><!-- contact-sidebar-body -->
    </div><!-- contact-sidebar -->

    <div class="contact-content">
      <div class="contact-content-header">
        <nav class="nav">
          <a href="#contactInformation" class="nav-link active" data-toggle="tab">Admin Information</a>
        </nav>
        <a href="" id="contactOptions" class="text-secondary mg-l-auto d-xl-none"><i data-feather="more-horizontal"></i></a>
      </div><!-- contact-content-header -->

      <div class="contact-content-body">
        <div class="tab-content">

          <div id="contactInformation" class="tab-pane show active pd-20 pd-xl-25">
            <div class="d-flex align-items-center justify-content-between mg-b-25">
              <h6 class="mg-b-0">Personal Details</h6>
              <?php if ($admin->role == 1) { ?>
                <div class="d-flex">
                  <a href="#modalEditAdmin" data-toggle="modal" class="btn btn-sm btn-white d-flex align-items-center mg-r-5"><i data-feather="edit-2"></i><span class="d-none d-sm-inline mg-l-5"> Edit</span></a>
                  <a href="#modalDeleteAdmin" data-toggle="modal" class="btn btn-sm btn-white d-flex align-items-center mg-r-5"><i data-feather="trash"></i><span class="d-none d-sm-inline mg-l-5"> Delete</span></a>
                  <a href="#modalNewAdmin" data-toggle="modal" class="btn btn-sm btn-primary d-flex align-items-center"><i data-feather="trash"></i><span class="d-none d-sm-inline mg-l-5"> Add</span></a>
                </div>
              <?php  }else{} ?>
            </div>

            <div class="row">
              <div class="col-6 col-sm">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Name</label>
                <p class="mg-b-0"><?php if ($uname != null) {echo $uname;}else{echo "----";} ?></p>
              </div><!-- col -->
            </div><!-- row -->

            <h6 class="mg-t-40 mg-b-20">Contact Details</h6>

            <div class="row row-sm">
              <div class="col-6">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Mobile Phone</label>
                <p class="tx-primary tx-rubik mg-b-0"><?php if ($uphone != null) {echo $uphone;}else{echo "----";} ?></p>
              </div>
              <div class="col-6">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Email Address</label>
                <p class="tx-primary mg-b-0"><?php if ($uemail != null) {echo $uemail;}else{echo "----";} ?></p>
              </div>
              <div class="col-6 mg-t-20 mg-sm-t-30">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Job Position</label>
                <p class="mg-b-0"><?php if ($uposition != null) {echo $uposition;}else{echo "----";} ?></p>
              </div>
            </div><!-- row -->
          </div>
        </div><!-- tab-content -->
      </div><!-- contact-content-body -->
    </div><!-- contact-content -->

  </div><!-- contact-wrapper -->
</div>