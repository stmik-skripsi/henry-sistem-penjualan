    <div class="content content-components">
      <div class="container">
        <div class="tx-14 mg-b-30">
          <h4 class="mg-b-0 tx-spacing--1">Ads Order Wizard</h4>
        </div>
        <div class="tx-13 mg-b-25">
          <form id="form" action="<?php echo site_url(); ?>client_area/generate_invoice_ads" method="POST">
            <div id="wizard1">
              <h3>Ads Target & Type</h3>
              <section>
                <p class="mg-b-20">Choose Your Platform and Target</p>
                <div class="row row-sm">
                  <div class="form-group col-sm-6">
                    <label>Ads Type <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="adstype" name="adstype" required>
                      <option value="" disabled selected="">Select Type</option>
                      <option value="Awareness">Awareness</option>
                      <option value="Interaction">Interaction</option>
                      <option value="Selling">Selling</option>
                    </select>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Platform <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="adsplatform" name="adsplatform" required>
                      <option value="" disabled selected="">Select Social Media Platform</option>
                      <option value="Facebook">Facebook</option>
                      <option value="Instagram">Instagram</option>
                      <option value="Google">Google</option>
                      <option value="Linked In">LinkedIn</option>
                    </select>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Age <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="adsage" name="adsage" required>
                      <option value="" disabled selected="">Select Age</option>
                      <option value="18 - 24">18 - 24</option>
                      <option value="25 - 34">25 - 34</option>
                      <option value="35 - 44">35 - 44</option>
                      <option value="35 - 44">35 - 44</option>
                      <option value="45 - 54">45 - 54</option>
                      <option value="55 - 64">55 - 64</option>
                      <option value="65 or more">65 or more</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Gender <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="adsgender" name="adsgender" required>
                      <option value="" disabled selected="">Select Day</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                      <option value="Male and Female">Male and Female</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Target Location <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Indonesia, Near Bali, etc" id="adslocation" name="adslocation" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Target Job <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="CEO, Manager, Owner, etc" id="adsjob" name="adsjob" required>
                  </div>
                </div><!-- form-row -->
              </section>
              <h3>Ads Budget and Duration</h3>
              <section>
                <p class="mg-b-20">Set your ads budget and duration </p>
                <div class="row row-sm">
                  <div class="form-group col-sm-6">
                    <label>Ads Budget <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Budget per day in Rupiah" id="adsbudget" name="adsbudget" required>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Ads Duration <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="minimal 14 days" id="adsduration" name="adsduration" required>
                  </div><!-- col -->
                </div><!-- row -->
              </section>
              <h3>Payment Details</h3>
              <section>
                <p class="mg-b-20"></p>
                <div class="row row-sm">
                  <div class="form-group col-6">
                    <label>Choose Your Payment Method <span class="tx-danger">*</span></label>
                    <select class="custom-select" required id="adspayment" name="adspayment">
                      <option value disabled selected="">Select Payment Method</option>
                      <option value="Paypal">Paypal</option>
                      <option value="Bank Transfer">Bank Transfer</option>
                    </select>
                  </div><!-- col -->
                </div><!-- row -->
              </section>
            </div>
          </form>
        </div><!-- df-example -->
      </div><!-- container -->
    </div><!-- content -->

    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/feather-icons/feather.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/prism.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/parsleyjs/parsley.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery-steps/build/jquery.steps.min.js"></script>

    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.js"></script>

    <script>
      $(function(){
        'use strict'

        $('#wizard1').steps({
          headerTag: 'h3',
          bodyTag: 'section',
          autoFocus: true,
          titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
          onStepChanging: function (event, currentIndex, newIndex){
            if(currentIndex < newIndex) {
              // Step 1 form validation
              if(currentIndex === 0) {
                var type = $('#adstype').parsley();
                var platform = $('#adsplatform').parsley();
                var age = $('#adsage').parsley();
                var gender = $('#adsgender').parsley();
                var location = $('#adslocation').parsley();
                var job = $('#adsjob').parsley();

                if(type.isValid() && platform.isValid() && age.isValid() && gender.isValid() && location.isValid() && job.isValid()) {
                  return true;
                } else {
                  type.validate();
                  platform.validate();
                  age.validate();
                  gender.validate();
                  location.validate();
                  job.validate();
                }
              }

              // Step 2 form validation
              if(currentIndex === 1) {
                var budget = $('#adsbudget').parsley();
                var duration = $('#adsduration').parsley();

                if(budget.isValid() && duration.isValid()) {
                  return true;
                } else {
                  budget.validate();
                  duration.validate();}
                }

              // Step 3 form validation
              if(currentIndex === 2) {
                var payment = $('#adspayment').parsley();

                if(payment.isValid()) {
                  return true;
                } else {
                  budget.validate();}
              } // Always allow step back to the previous step even if the current step is not valid.

            } else { return true; }},
            onFinished: function (event, currentIndex) {
              $("#form").submit();
            },
          });
      });
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
      })();
    </script>
    <!--End of Tawk.to Script-->
  </body>
  </html>
