<?php
$email = $this->session->userdata('email');
$query = $this->db->query("SELECT * FROM user where email = '".$email."'");
$user = $query->row();
$uid = $user->id;
?>
<div class="content content-fixed">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
      <div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Invoice List</li>
          </ol>
        </nav>
        <h4 class="mg-b-0 tx-spacing--1">Invoice List</h4>
      </div>
    </div>

    <div class="row row-xs">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table table-primary table-hover table-striped mg-b-0">
            <thead>
              <tr>
                <th scope="col">No</th>
                <th scope="col">Invoice Number</th>
                <th scope="col">Services</th>
                <th scope="col">Amount</th>
                <th scope="col">Due Date</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $query = $this->db->query("SELECT * FROM invoice WHERE iduser = '".$uid."'");
              $no = 1; foreach ($query->result_array() as $q) { ?>
                <tr>
                  <th scope="row" style="vertical-align: middle;"><?php echo $no; ?></th>
                  <td style="vertical-align: middle;"><?php echo $q['invoicenum']; ?></td>
                  <td style="vertical-align: middle;"><?php echo $q['servicesname']; ?></td>
                  <td style="vertical-align: middle;">Rp <?php echo number_format($q['amount']); ?></td>
                  <td style="vertical-align: middle;"><?php
                  $date = $q['date'];
                  $duedate = strtotime($date);
                  $duedate = strtotime("+7 day", $duedate);
                  echo date('F d, Y', $duedate);
                  ?></td>
                  <td style="vertical-align: middle;" class="<?php if ($q['status'] == "UNPAID") {echo "tx-danger";}elseif($q['status'] == "PENDING CONFIRMATION"){echo "tx-warning";}elseif($q['status'] == "PAID"){echo "tx-success";}?>"><?php echo $q['status']; ?></td>
                  <td style="vertical-align: middle;">
                    <a href="<?php echo site_url(); ?>client_area/invoice/<?php echo $q['invoicenum']; ?>" class="btn btn-sm pd-x-15 btn-white btn-uppercase mg-l-5"><i data-feather="file" class="wd-10 mg-r-5"></i> Details</a>
                    <?php if ($q['status'] == "UNPAID") { ?>
                      <a href="<?php echo site_url(); ?>client_area/confirm_payment/<?php echo $q['invoicenum']; ?>" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="file" class="wd-10 mg-r-5"></i> Confirm Payment</a>
                    <?php } ?>
                    <?php if ($q['status'] == "PAID") { ?>
                      <a href="<?php echo site_url(); ?>client_area/services/<?php echo $q['invoicenum']; ?>" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5"><i data-feather="settings" class="wd-10 mg-r-5"></i> See Services</a>
                    <?php } ?>
                  </td>
                </tr>
              <?php }; $no++; ?>
            </tbody>
          </table>
        </div>
      </div><!-- col -->
    </div><!-- container -->
  </div><!-- content -->
</div>

<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery/jquery.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/feather-icons/feather.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.stack.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery.flot/jquery.flot.resize.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/chart.js/Chart.bundle.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/jqvmap/maps/jquery.vmap.usa.js"></script>

<script src="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/prism.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/parsleyjs/parsley.min.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery-steps/build/jquery.steps.min.js"></script>

<script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.sampledata.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/js/dashboard-one.js"></script>

<!-- append theme customizer -->
<script src="<?php echo site_url(); ?>assets/dashboard/lib/js-cookie/js.cookie.js"></script>
<script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.settings.js"></script>

<?php if ($this->session->userdata('status')!="loggedin") { echo "";} else { ?> 
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
      var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
      s1.async=true;
      s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
      s1.charset='UTF-8';
      s1.setAttribute('crossorigin','*');
      s0.parentNode.insertBefore(s1,s0);
    })();
  </script>
  <!--End of Tawk.to Script-->
<?php } ?>
</body>
</html>
