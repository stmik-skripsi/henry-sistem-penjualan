<?php
$duedate = strtotime($date);
$duedate = strtotime("+7 day", $duedate);
?>
<div class="content content-fixed bd-b">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="d-sm-flex align-items-center justify-content-between">
      <div>
        <h4 class="mg-b-5">Invoice #<?php echo $invoicenum; ?></h4>
        <p class="mg-b-0 tx-color-03">Due on <?php echo date('F d, Y', $duedate); ?></p>
      </div>
      <div class="mg-t-20 mg-sm-t-0">
        <a href="<?php echo site_url(); ?>admin_area/set_paid/<?php echo $invoicenum; ?>/ads" class="btn btn-sm pd-x-15 btn-uppercase mg-r-10 <?php if ($status == "PAID") { echo "btn-success";}else{ echo "btn-outline-success";} ?>">PAID</a>
        <a href="<?php echo site_url(); ?>admin_area/set_unpaid/<?php echo $invoicenum; ?>/ads" class="btn btn-sm pd-x-15 btn-uppercase mg-r-10 <?php if ($status == "UNPAID") { echo "btn-danger";}else{ echo "btn-outline-danger";} ?>">UNPAID</a>
        <a href="<?php echo site_url(); ?>admin_area/set_pending/<?php echo $invoicenum; ?>/ads" class="btn btn-sm pd-x-15 btn-uppercase mg-r-10 <?php if ($status == "PENDING CONFIRMATION") { echo "btn-warning";}else{ echo "btn-outline-warning";} ?>">PENDING CONFIRMATION</a>
      </div>
    </div>
  </div><!-- container -->
</div><!-- content -->

<div class="content tx-13">
  <div class="container pd-x-0 pd-lg-x-10 pd-xl-x-0">
    <div class="row">
      <div class="col-sm-6 col-lg-4">
        <h6 class="tx-15 mg-b-10">Invoice Information</h6>
        <ul class="list-unstyled lh-7">
          <li class="d-flex justify-content-between">
            <span>Invoice Number</span>
            <span>#<?php echo $invoicenum; ?></span>
          </li>
          <li class="d-flex justify-content-between">
            <span>Issue Date</span>
            <span><?php echo $date; ?></span>
          </li>
          <li class="d-flex justify-content-between">
            <span>Due Date</span>
            <span><?php echo date('F d, Y', $duedate); ?></span>
          </li>
          <li class="d-flex justify-content-between">
            <span>Payment Method</span>
            <span><?php echo $method; ?></span>
          </li>
        </ul>
      </div><!-- col -->
      <div class="col-sm-6 col-lg-8 tx-right d-none d-md-block">
        <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Status</label>
        <h1 class="tx-normal <?php if ($status == "UNPAID") {echo "tx-danger";}elseif($status == "PENDING CONFIRMATION"){echo "tx-warning";}elseif($status == "PAID"){echo "tx-success";}?> mg-b-10 tx-spacing--2"><?php echo $status; ?></h1>
      </div><!-- col -->      
    </div><!-- row -->

    <div class="table-responsive mg-t-20 mg-b-20">
      <table class="table table-invoice bd-b">
        <thead>
          <tr>
            <th class="wd-40p d-none d-sm-table-cell">Description</th>
            <th class="tx-right">Amount</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="d-none d-sm-table-cell">
              Services : <?php echo $services; ?></br>
              <?php if ($services == "SEO") { ?>
                SEO Article per week : <?php echo $seoperweek; ?> articles</br>
                SEO Duration : <?php echo $seoduration; ?> Months
              <?php }  ?>
              <?php if ($services == "Website") { ?>
                Website Type : <?php echo $webtype; ?></br>
                Website Domain : <?php echo $webdomain; ?>
              <?php }  ?>
              <?php if ($services == "Ads") { ?>
                Ads Type : <?php echo $adstype; ?></br>
                Ads Platform : <?php echo $adsplatform; ?></br>
                Ads Budget per day : Rp <?php echo number_format($adsbudget); ?></br>
                Ads Duration : <?php echo $adsduration; ?> days
              <?php }  ?>
            </td>
            <td class="tx-right">Rp <?php echo number_format($amount); ?></td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="row justify-content-between">
      <div class="col-sm-6 col-lg-6 order-2 order-sm-0 mg-t-40 mg-sm-t-0">
        <label class="tx-sans tx-uppercase tx-10 tx-medium tx-spacing-1 tx-color-03">Notes</label>
        <p>Kindly pay your invoice within 7 days or your invoice will be cancelled.</p>
      </div><!-- col -->
      <div class="col-sm-6 col-lg-4 order-1 order-sm-0">
        <ul class="list-unstyled lh-7 pd-r-10">
          <li class="d-flex justify-content-between">
            <strong>Total Due</strong>
            <strong>Rp <?php echo number_format($amount); ?></strong>
          </li>
        </ul>
      </div><!-- col -->
    </div><!-- row -->
  </div><!-- container -->
</div><!-- content -->