<div class="content-body pd-0">
  <div class="contact-wrapper contact-wrapper-two">
    <div class="contact-sidebar" style="left: 0px; width: 340px">
      <div class="contact-sidebar-body" style="top: 0px">
        <div class="tab-content">
          <div id="tabContact" class="tab-pane fade active show">
            <div class="pd-y-20 pd-x-10 contact-list">
              <?php
              $query = $this->db->query('SELECT * FROM user');
              $no = 1; foreach ($query->result_array() as $q) { ?>
                <div class="media <?php if ( $uid == $q['id']) {echo "active";} ?>" onclick="location.href='<?php echo site_url(); ?>admin_area/user_list/<?php echo $q['id']; ?>';">
                  <div class="avatar avatar-sm avatar-online"><span class="avatar-initial rounded-circle bg-gray-700"><i data-feather="user"></i></span></div>
                  <div class="media-body mg-l-10">
                    <h6 class="tx-13 mg-b-3"><?php echo $q['fname']."&nbsp;".$q['lname'] ; ?></h6>
                    <span class="tx-12"><?php echo $q['email']; ?></span>
                  </div><!-- media-body -->
                  <nav>
                    <a href="<?php echo site_url(); ?>admin_area/user_list/<?php echo $q['id']; ?>" data-toggle="tooltip" title="View Data"><i data-feather="eye"></i></a>
                  </nav>
                </div><!-- media -->
              <?php } ?>
            </div><!-- contact-list -->
          </div><!-- tab-pane -->
          <div id="tabExport" class="tab-pane fade">
            <div class="pd-y-25 pd-x-20">
              <h6 class="tx-12 tx-semibold tx-spacing-1 tx-uppercase">Export Contacts</h6>
              <p class="tx-13 tx-color-03 mg-b-20">You can export your contacts and import them into other email apps.</p>
              <div class="form-group">
                <label class="tx-13">Which contacts do you want to export?</label>
                <select class="custom-select tx-13">
                  <option value="1" selected>All Contacts</option>
                  <option value="2">My Favorites</option>
                  <option value="3">My Family</option>
                  <option value="4">My Friends</option>
                  <option value="4">My Officemates</option>
                </select>
              </div><!-- form-group -->
              <button class="btn btn-sm btn-primary">Export</button>
            </div>
          </div><!-- tab-pane -->
        </div><!-- tab-content -->
      </div><!-- contact-sidebar-body -->
    </div><!-- contact-sidebar -->

    <div class="contact-content">
      <div class="contact-content-header">
        <nav class="nav">
          <a href="#contactInformation" class="nav-link active" data-toggle="tab">User Information</a>
        </nav>
        <a href="" id="contactOptions" class="text-secondary mg-l-auto d-xl-none"><i data-feather="more-horizontal"></i></a>
      </div><!-- contact-content-header -->

      <div class="contact-content-body">
        <div class="tab-content">
          <div id="contactInformation" class="tab-pane show active pd-20 pd-xl-25">
            <div class="d-flex align-items-center justify-content-between mg-b-25">
              <h6 class="mg-b-0">Personal Details</h6>
              <div class="d-flex">
                <a href="#modalEditUser" data-toggle="modal" class="btn btn-sm btn-white d-flex align-items-center mg-r-5"><i data-feather="edit-2"></i><span class="d-none d-sm-inline mg-l-5"> Edit</span></a>
                <a href="#modalDeleteUser" data-toggle="modal" class="btn btn-sm btn-white d-flex align-items-center mg-r-5"><i data-feather="trash"></i><span class="d-none d-sm-inline mg-l-5"> Delete</span></a>
                <a href="#modalNewUser" data-toggle="modal" class="btn btn-sm btn-primary d-flex align-items-center"><i data-feather="trash"></i><span class="d-none d-sm-inline mg-l-5"> Add</span></a>
              </div>
            </div>

            <div class="row">
              <div class="col-6 col-sm">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Firstname</label>
                <p class="mg-b-0"><?php if ($ufname != null) {echo $ufname;}else{echo "----";} ?></p>
              </div><!-- col -->
              <div class="col-6 mg-t-20 mg-sm-t-0">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Lastname</label>
                <p class="mg-b-0"><?php if ($ulname != null) {echo $ulname;}else{echo "----";} ?></p>
              </div><!-- col -->
            </div><!-- row -->

            <h6 class="mg-t-40 mg-b-20">Contact Details</h6>

            <div class="row row-sm">
              <div class="col-6">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Mobile Phone</label>
                <p class="tx-primary tx-rubik mg-b-0"><?php if ($uphone != null) {echo $uphone;}else{echo "----";} ?></p>
              </div>
              <div class="col-6">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Email Address</label>
                <p class="tx-primary mg-b-0"><?php if ($uemail != null) {echo $uemail;}else{echo "----";} ?></p>
              </div>
              <div class="col-6 mg-t-20 mg-sm-t-30">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Company</label>
                <p class="mg-b-0"><?php if ($ucompany != null) {echo $ucompany;}else{echo "----";} ?></p>
              </div>
              <div class="col-6 mg-t-20 mg-sm-t-30">
                <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Job Position</label>
                <p class="mg-b-0"><?php if ($uposition != null) {echo $uposition;}else{echo "----";} ?></p>
              </div>
            </div><!-- row -->
          </div>
          <div id="contactLogs" class="tab-pane pd-20 pd-xl-25">
            <div class="d-flex align-items-center justify-content-between mg-b-30">
              <h6 class="tx-15 mg-b-0">Call &amp; Message Logs</h6>
              <a href="#" class="btn btn-sm btn-white d-flex align-items-center"><i class="icon ion-md-time mg-r-5 tx-16 lh--9"></i> Clear History</a>
            </div>
          </div><!-- tab-pane -->
        </div><!-- tab-content -->
      </div><!-- contact-content-body -->

      <div class="contact-content-sidebar">
        <div class="clearfix mg-b-25">
          <div id="contactAvatar" class="pos-relative float-left">
            <div class="avatar avatar-xl"><span class="avatar-initial rounded-circle bg-gray-700 tx-normal">A</span></div>
            <a href="" class="contact-edit-photo" data-toggle="tooltip" title="Upload Photo"><i data-feather="edit-2"></i></a>
          </div>
        </div>
        <h5 id="contactName" class="tx-18 tx-xl-20 mg-b-5">Abigail Johnson</h5>
        <p class="tx-13 tx-lg-12 tx-xl-13 tx-color-03 mg-b-20">President &amp; CEO - ThemePixels, Inc.</p>
        <nav class="contact-call-nav mg-b-20">
          <a href="#" class="nav-call" data-toggle="tooltip" title="Make a Phone Call"><i data-feather="phone"></i></a>
          <a href="#" class="nav-video" data-toggle="tooltip" title="Make a Video Call"><i data-feather="video"></i></a>
          <a href="#" class="nav-msg" data-toggle="tooltip" title="Send Message"><i data-feather="message-square"></i></a>
        </nav><!-- contact-call-nav -->

        <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-10">Biography</label>
        <p class="tx-13 mg-b-0">Gambler, Tea Drinker, Ultimate Piggie, Replacement President of a Major Soft Drink Manufacturer. When I give out candies, I give people the flavour I don't like. </p>

        <hr class="mg-y-20">

        <label class="tx-10 tx-medium tx-spacing-1 tx-color-03 tx-uppercase tx-sans mg-b-15">Options</label>
        <nav class="nav flex-column contact-content-nav mg-b-25">
          <a href="" class="nav-link"><i data-feather="share"></i> Share this Contact</a>
          <a href="" class="nav-link"><i data-feather="star"></i> Add to Favorites</a>
          <a href="" class="nav-link"><i data-feather="slash"></i> Block this Contact</a>
        </nav>

      </div><!-- contact-content-sidebar -->
    </div><!-- contact-content -->

  </div><!-- contact-wrapper -->
</div>