    <div class="content content-components">
      <div class="container">
        <div class="tx-14 mg-b-30">
          <h4 class="mg-b-0 tx-spacing--1">SEO Order Wizard</h4>
        </div>
        <div class="tx-13 mg-b-25">
          <form id="form" action="<?php echo site_url(); ?>client_area/generate_invoice_seo" method="POST">
            <div id="wizard1">
              <h3>SEO Target & Details</h3>
              <section>
                <p class="mg-b-20"></p>
                <div class="row row-sm">
                  <div class="form-group col-sm-6">
                    <label>Your Website <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Your website" id="seowebsite" name="seowebsite" required>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Main Topic <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Your article main topic" id="seotopic" name="seotopic" required>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Keyword Preferences<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Your keyword preferences" id="seokeyword" name="seokeyword" required>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Total Article per week <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="seoperweek" name="seoperweek" required>
                      <option value="" disabled selected="">How much article per week</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                    </select>
                  </div>
                  <div class="form-group col-sm-6">
                    <label>Duration <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="seoduration" name="seoduration" required>
                      <option value="" disabled selected="">SEO Services Duration</option>
                      <option value="3">3 Month</option>
                      <option value="6">6 Month</option>
                      <option value="12">12 Month</option>
                    </select>
                  </div>
                </div><!-- form-row -->
              </section>
              <h3>Payment Details</h3>
              <section>
                <p class="mg-b-20"></p>
                <div class="row row-sm">
                  <div class="form-group col-6">
                    <label>Choose Your Payment Method <span class="tx-danger">*</span></label>
                    <select class="custom-select" required id="seopayment" name="seopayment">
                      <option value="" disabled selected="">Select Payment Method</option>
                      <option value="Paypal">Paypal</option>
                      <option value="Bank Transfer">Bank Transfer</option>
                    </select>
                  </div><!-- col -->
                  <?php
                  $email = $this->session->userdata('email');
                  $query = $this->db->query("SELECT * FROM user where email = '".$email."'");
                  $user = $query->row();
                  if ($user->phone == '') { ?>
                    <div class="form-group col-6">
                      <label>Phone</label>
                      <input type="text" class="form-control" placeholder="Your phone / whatsapp number" name="phone" id="phone">
                    </div><!-- col -->
                  <?php } ?>
                </div><!-- row -->
              </section>
            </div>
          </form>
        </div><!-- df-example -->
      </div><!-- container -->
    </div><!-- content -->

    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/feather-icons/feather.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/prism.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/parsleyjs/parsley.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery-steps/build/jquery.steps.min.js"></script>

    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.js"></script>

    <script>
      $(function(){
        'use strict'

        $('#wizard1').steps({
          headerTag: 'h3',
          bodyTag: 'section',
          autoFocus: true,
          titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
          onStepChanging: function (event, currentIndex, newIndex){
            if(currentIndex < newIndex) {
              // Step 1 form validation
              if(currentIndex === 0) {
                var seowebsite = $('#seowebsite').parsley();
                var seotopic = $('#seotopic').parsley();
                var seokeyword = $('#seokeyword').parsley();
                var seoperweek = $('#seoperweek').parsley();
                var seoduration = $('#seoduration').parsley();

                if(seowebsite.isValid() && seotopic.isValid() && seokeyword.isValid() && seoperweek.isValid() && seoduration.isValid()) {
                  return true;
                } else {
                  seowebsite.validate();
                  seotopic.validate();
                  seokeyword.validate();
                  seoperweek.validate();
                  seoduration.validate();
                }
              }

              // Step 2 form validation
              if(currentIndex === 1) {
                var seopayment = $('#seopayment').parsley();

                if(payment.isValid()) {
                  return true;
                } else {
                  seopayment.validate();}
              } // Always allow step back to the previous step even if the current step is not valid.

            } else { return true; }},
            onFinished: function (event, currentIndex) {
              $("#form").submit();
            },
          });
      });
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
      })();
    </script>
    <!--End of Tawk.to Script-->
  </body>
  </html>
