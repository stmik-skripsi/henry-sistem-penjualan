
<div class="content content-auth">
  <div class="container">
    <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
      <div class="media-body align-items-center d-none d-lg-flex">
        <div class="mx-wd-600">
          <img src="<?php echo site_url(); ?>assets/dashboard/img/img16.png" class="img-fluid" alt="">
        </div>
      </div><!-- media-body -->
      <div class="sign-wrapper mg-lg-l-50 mg-x l-l-60">
        <div class="wd-100p">
          <h4 class="tx-color-01 mg-b-5">Create New Account</h4>
          <p class="tx-color-03 tx-16 mg-b-40">It's free to signup and only takes a minute.</p>
          <form action="<?php echo site_url(); ?>client_area/auth2" method="POST">
            <div class="form-group">
              <label>First Name</label>
              <input name="fname" type="text" class="form-control" placeholder="Enter your firstname">
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input name="lname" type="text" class="form-control" placeholder="Enter your lastname">
            </div>
            <div class="form-group">
              <label>Email address</label>
              <input name="email" type="email" class="form-control" placeholder="yourname@yourmail.com">
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-between mg-b-5">
                <label class="mg-b-0-f">Password</label>
              </div>
              <input name="password" type="password" class="form-control" placeholder="Enter your password">
            </div>
            <div class="form-group tx-12">
              By clicking <strong>Create an account</strong> below, you agree to our terms of service and privacy statement.
            </div><!-- form-group -->
            <button class="btn btn-brand-02 btn-block">Create Account</button>
          </form>
          <div class="divider-text">or</div>
          <div class="tx-13 mg-t-20 tx-center">Already have an account? <a href="<?php echo site_url(); ?>client_area/login">Sign In</a></div>
        </div>
      </div><!-- sign-wrapper -->
    </div><!-- sign-wrapper -->
  </div><!-- media -->
</div><!-- container -->
</div><!-- content -->
