
<div class="content content-auth">
  <div class="container">
    <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
      <div class="media-body align-items-center d-none d-lg-flex">
        <div class="mx-wd-600">
          <img src="<?php echo site_url(); ?>assets/dashboard/img/img15.png" class="img-fluid" alt="">
        </div>
      </div><!-- media-body -->
      <div class="sign-wrapper mg-lg-l-50 mg-x l-l-60">
        <div class="wd-100p">
          <h3 class="tx-color-01 mg-b-5">Sign In</h3>
          <p class="tx-color-03 tx-16 mg-b-40">Welcome back! Please signin to continue.</p>
          <form action="<?php echo site_url(); ?>admin_area/auth" method="POST">
            <div class="form-group">
              <label>Email address</label>
              <input name="email" type="email" class="form-control" placeholder="yourname@yourmail.com">
            </div>
            <div class="form-group">
              <div class="d-flex justify-content-between mg-b-5">
                <label class="mg-b-0-f">Password</label>
                <a href="" class="tx-13">Forgot password?</a>
              </div>
              <input name="password" type="password" class="form-control" placeholder="Enter your password">
            </div>
            <button class="btn btn-brand-02 btn-block" id="login">Sign In</button>
          </form>
          <div class="divider-text">or</div>
          <div class="tx-13 mg-t-20 tx-center">Don't have an account? <a href="<?php echo site_url(); ?>client_area/register">Create an Account</a></div>
        </div>
      </div><!-- sign-wrapper -->
    </div><!-- media -->
  </div><!-- container -->
</div><!-- content -->
