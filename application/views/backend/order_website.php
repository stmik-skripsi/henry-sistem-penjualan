    <div class="content content-components">
      <div class="container">
        <div class="tx-14 mg-b-30">
          <h4 class="mg-b-0 tx-spacing--1">Website Order Wizard</h4>
        </div>
        <div class="tx-13 mg-b-25">
          <form id="form" action="<?php echo site_url(); ?>client_area/generate_invoice_web" method="POST">
            <div id="wizard1">
              <h3>Website Type</h3>
              <section>
                <p class="mg-b-20">Choose Your Website Type</p>
                <div class="row row-sm">
                  <div class="form-group col-sm-6">
                    <label>Website Type <span class="tx-danger">*</span></label>
                    <select class="custom-select" id="webtype" name="webtype" required>
                      <option value="" disabled selected="">Select Type</option>
                      <option value="One Page">One Page</option>
                      <option value="Company Profile">Company Profile</option>
                      <option value="Online Shop">Online Shop</option>
                    </select>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Domain <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="domain.com" id="webdomain" name="webdomain" required>
                  </div><!-- col -->
                </div><!-- form-row -->
              </section>
              <h3>Website Details</h3>
              <section>
                <p class="mg-b-20">Your Website Details</p>
                <div class="row row-sm">
                  <div class="form-group col-sm-6">
                    <label>Website Theme Color<span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Your website main color" id="webcolor" name="webcolor" required>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Website Name <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Your website name" id="webname" name="webname" required>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>References <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Your website references" id="webref" name="webref" required>
                  </div><!-- col -->
                  <div class="form-group col-sm-6">
                    <label>Brief Link <span class="tx-danger">*</span></label>
                    <input type="text" class="form-control" placeholder="Put your google drive website brief link here" id="webbrief" name="webbrief" required>
                  </div><!-- col -->
                </div><!-- row -->
              </section>
              <h3>Contact & Payment Details</h3>
              <section>
                <p class="mg-b-20"></p>
                <div class="row row-sm">
                  <div class="form-group col-6">
                    <label>Choose Your Payment Method <span class="tx-danger">*</span></label>
                    <select class="custom-select" required id="webpayment" name="webpayment">
                      <option value="" disabled selected="">Select Payment Method</option>
                      <option value="Paypal">Paypal</option>
                      <option value="Bank Transfer">Bank Transfer</option>
                    </select>
                  </div><!-- col -->
                  <?php
                  $email = $this->session->userdata('email');
                  $query = $this->db->query("SELECT * FROM user where email = '".$email."'");
                  $user = $query->row();
                  if ($user->phone == '') { ?>
                    <div class="form-group col-6">
                      <label>Phone</label>
                      <input type="text" class="form-control" placeholder="Your phone / whatsapp number" name="phone" id="phone">
                    </div><!-- col -->
                  <?php } ?>
                </div><!-- row -->
              </section>
            </div>
          </form>
        </div><!-- df-example -->
      </div><!-- container -->
    </div><!-- content -->

    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/feather-icons/feather.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/prismjs/prism.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/parsleyjs/parsley.min.js"></script>
    <script src="<?php echo site_url(); ?>assets/dashboard/lib/jquery-steps/build/jquery.steps.min.js"></script>

    <script src="<?php echo site_url(); ?>assets/dashboard/js/dashforge.js"></script>

    <script>
      $(function(){
        'use strict'

        $('#wizard1').steps({
          headerTag: 'h3',
          bodyTag: 'section',
          autoFocus: true,
          titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
          onStepChanging: function (event, currentIndex, newIndex){
            if(currentIndex < newIndex) {
              // Step 1 form validation
              if(currentIndex === 0) {
                var webtype = $('#webtype').parsley();
                var webdomain = $('#webdomain').parsley();

                if(webtype.isValid() && webdomain.isValid()) {
                  return true;
                } else {
                  webtype.validate();
                  webdomain.validate();
                }
              }

              // Step 2 form validation
              if(currentIndex === 1) {
                var webcolor = $('#webcolor').parsley();
                var webname = $('#webname').parsley();
                var webref = $('#webref').parsley();
                var webbrief = $('#webbrief').parsley();

                if(webcolor.isValid() && webname.isValid() && webref.isValid() && webbrief.isValid()) {
                  return true;
                } else {
                  webcolor.validate();
                  webname.validate();
                  webref.validate();
                  webbrief.validate();
                }
              }

              // Step 3 form validation
              if(currentIndex === 2) {
                var payment = $('#webpayment').parsley();

                if(payment.isValid() ) {
                  return true;
                } else {
                  payment.validate();
                }
              } // Always allow step back to the previous step even if the current step is not valid.

            } else { return true; }},
            onFinished: function (event, currentIndex) {
              $("#form").submit();
            },
          });
      });
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
      var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
      (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5eed8c834a7c6258179afd30/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
      })();
    </script>
    <!--End of Tawk.to Script-->
  </body>
  </html>
