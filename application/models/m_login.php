<?php 
 
class M_login extends CI_Model{

	function __construct()
	{
		parent::__construct(); // construct the Model class
	}

	function login_check($table,$where){		
		return $this->db->get_where($table,$where);
	}	
	
}